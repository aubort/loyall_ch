+++
date = "2017-07-23T11:46:20Z"

identifier="terms"
title = "Terms & Conditions | Loyall Cloud Solutions"
pagetitle = "Terms and Conditions (German only)"
+++

## Kontaktadresse

Loyall GmbH  
Seestrasse 124  
8802 Kilchberg (Zurich)  
Schweiz  

[hello@loyall.ch](mailto:hello@loyall.ch)  
+41 44 585 33 00


### **Vertretungsberechtigte Personen**  
Pascal Aubort


Handelsregistereintrag Nummer:  CHE-221.389.590  
Mehrwertsteuernummer CHE-221.389.590 MWST

### **Bank**  
PostFinance SA  
IBAN: CH14 0900 0000 3037 9914 1 

## Haftungsausschluss

Der Autor übernimmt keinerlei Gewähr hinsichtlich der  inhaltlichen Richtigkeit, Genauigkeit, Aktualität, Zuverlässigkeit und  Vollständigkeit der Informationen.
Haftungsansprüche gegen den Autor wegen Schäden materieller  oder immaterieller Art, welche aus dem Zugriff oder der Nutzung bzw.  Nichtnutzung der veröffentlichten Informationen, durch Missbrauch der  Verbindung oder durch technische Störungen entstanden sind, werden  ausgeschlossen.
Alle  Angebote sind unverbindlich. Der Autor behält es sich ausdrücklich vor, Teile  der Seiten oder das gesamte Angebot ohne gesonderte Ankündigung zu verändern,  zu ergänzen, zu löschen oder die Veröffentlichung zeitweise oder endgültig  einzustellen.


## Haftung für Links

Verweise und Links auf Webseiten  Dritter liegen ausserhalb unseres Verantwortungsbereichs Es wird jegliche Verantwortung für solche Webseiten  abgelehnt.  Der Zugriff und die Nutzung solcher Webseiten erfolgen  auf eigene Gefahr des Nutzers oder der Nutzerin. 


## Urheberrechte

Die Urheber- und alle anderen Rechte an Inhalten, Bildern, Fotos oder anderen Dateien auf der Website gehören ausschliesslich der Firma Loyall oder den speziell genannten  Rechtsinhabern. Für die Reproduktion jeglicher Elemente ist die schriftliche Zustimmung der Urheberrechtsträger im Voraus einzuholen.


## Datenschutz

Gestützt auf Artikel 13 der schweizerischen Bundesverfassung  und die datenschutzrechtlichen Bestimmungen des Bundes (Datenschutzgesetz, DSG)  hat jede Person Anspruch auf Schutz ihrer Privatsphäre sowie auf Schutz vor  Missbrauch ihrer persönlichen Daten. Wir halten diese Bestimmungen ein.  Persönliche Daten werden streng vertraulich behandelt und weder an Dritte  verkauft noch weiter gegeben.
In enger Zusammenarbeit mit unseren Hosting-Providern  bemühen wir uns, die Datenbanken so gut wie möglich vor fremden Zugriffen, Verlusten,  Missbrauch oder vor Fälschung zu schützen.
Beim Zugriff auf unsere Webseiten werden folgende Daten in  Logfiles gespeichert: IP-Adresse, Datum, Uhrzeit, Browser-Anfrage und allg.  übertragene Informationen zum Betriebssystem resp. Browser. Diese Nutzungsdaten  bilden die Basis für statistische, anonyme Auswertungen, so dass Trends  erkennbar sind, anhand derer wir unsere Angebote entsprechend verbessern  können. 


## Datenschutzerklärung für die Nutzung von Google Analytics

Diese Website benutzt Google Analytics, einen Webanalysedienst der  Google Inc. ("Google"). Google Analytics verwendet sog.  "Cookies", Textdateien, die auf Ihrem Computer gespeichert werden und  die eine Analyse der Benutzung der Website durch Sie ermöglichen. Die durch den  Cookie erzeugten Informationen über Ihre Benutzung dieser Website werden in der  Regel an einen Server von Google in den USA übertragen und dort gespeichert. Im  Falle der Aktivierung der IP-Anonymisierung auf dieser Webseite wird Ihre  IP-Adresse von Google jedoch innerhalb von Mitgliedstaaten der Europäischen  Union oder in anderen Vertragsstaaten des Abkommens über den Europäischen Wirtschaftsraum  zuvor gekürzt. 
Nur in Ausnahmefällen wird die volle IP-Adresse an einen Server von  Google in den USA übertragen und dort gekürzt. Google wird diese  Informationen benutzen, um Ihre Nutzung der Website auszuwerten, um Reports  über die Websiteaktivitäten für die Websitebetreiber zusammenzustellen und um  weitere mit der Websitenutzung und der Internetnutzung verbundene  Dienstleistungen zu erbringen. Auch wird Google diese Informationen  gegebenenfalls an Dritte übertragen, sofern dies gesetzlich vorgeschrieben oder  soweit Dritte diese Daten im Auftrag von Google verarbeiten. Die im  Rahmen von Google Analytics von Ihrem Browser übermittelte IP-Adresse wird  nicht mit anderen Daten von Google zusammengeführt. 
Sie können die Installation der Cookies durch eine  entsprechende Einstellung Ihrer Browser Software verhindern; wir weisen Sie  jedoch darauf hin, dass Sie in diesem Fall gegebenenfalls nicht sämtliche  Funktionen dieser Website voll umfänglich nutzen können. Durch  die Nutzung dieser Website erklären Sie sich mit der Bearbeitung der über Sie  erhobenen Daten durch Google in der zuvor beschriebenen Art und Weise und zu  dem zuvor benannten Zweck einverstanden.


## Datenschutzerklärung für die Nutzung von Google +1

Mithilfe der Google +1-Schaltfläche können Sie Informationen weltweit  veröffentlichen. Über die Google +1-Schaltfläche erhalten Sie und andere Nutzer  personalisierte Inhalte von Google und dessen Partnern. Google speichert sowohl  die Information, dass Sie für einen Inhalt +1 gegeben haben, als auch  Informationen über die Seite, die Sie beim Klicken auf +1 angesehen haben. Ihre  +1 können als Hinweise zusammen mit Ihrem Profilnamen und Ihrem Foto in  Google-Diensten, wie etwa in Suchergebnissen oder in Ihrem Google-Profil, oder  an anderen Stellen auf Websites und Anzeigen im Internet eingeblendet werden. 
Google zeichnet Informationen über Ihre +1-Aktivitäten auf, um die  Google-Dienste für Sie und andere zu verbessern. 
Um die Google +1-Schaltfläche verwenden zu können, benötigen Sie ein  weltweit sichtbares, öffentliches Google-Profil, das zumindest den für das  Profil gewählten Namen enthalten muss. Dieser Name wird in allen  Google-Diensten verwendet. In manchen Fällen kann dieser Name auch einen  anderen Namen ersetzen, den Sie beim Teilen von Inhalten über Ihr Google-Konto  verwendet haben. Die Identität Ihres Google-Profils kann Nutzern angezeigt  werden, die Ihre E-Mail-Adresse kennen oder über andere identifizierende  Informationen von Ihnen verfügen. 
Neben den oben erläuterten Verwendungszwecken werden die von Ihnen  bereitgestellten Informationen gemäß den geltenden  Google-Datenschutzbestimmungen (http://www.google.com/intl/de/policies/privacy/)  genutzt. Google veröffentlicht möglicherweise zusammengefasste  Statistiken über die +1-Aktivitäten der Nutzer bzw. geben diese Statistiken an  unsere Nutzer und Partner weiter, wie etwa Publisher, Inserenten oder  verbundene Websites. 


## Quelle

Dieses Impressum wurde am 13.04.2017 mit dem  Impressum-Generator http://www.bag.ch/impressum-generator der Firma Brunner Medien AG in Kriens erstellt. Die Brunner Medien AG in Kriens übernimmt keine  Haftung. 

Bilder: Plutschow Gallery, 123rf.com, shutterstock.com, unsplash.com Icons: Material Design
