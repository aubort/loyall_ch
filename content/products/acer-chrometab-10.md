---
date: "2018-05-15T18:53:23Z"
weight: 1

id: '1'
mfg: 'Acer'
family: 'Chromebook'
price: '999'
name: 'Chromebook Tab 10'
serie: 'D651N-K9WT'
cover_image: 'https://static.acer.com/up/Resource/Acer/Laptops/Chromebook_Tab_10/Photogallery/20180328/Acer-Chromebook-Tab-10-D651N-photogallery-01.png'
desc_link: 'https://www.acer.com/ac/en/US/content/series/acerchromebooktab10'
specs_link: 'https://www.acer.com/ac/en/US/content/model/NX.H0BAA.001'
play_support: 'Stable'
summary: '10'' Touchscreen, 4 GB RAM, 32 GB, Wacom Stylus Pen'
display: '9.7", QXGA (2048 x 1536) resolution, IPS technology'
display_angle: ''
display_touch: ''
stylus: 'Wacom® EMR Pen'
ram: 'LPDDR3 4 GB (standard)'
cpu: 'Rockchip RK3399, 2 GHz'
graphic: ''
storage: '32 GB'
power_supply: ''
interface: ''
battery: ''
dimensions: '6.78" x 9.38" x 0.39"'
device_weight: '0.5 kg'
options:
    - playstore
    - stylus
    - rugged
---