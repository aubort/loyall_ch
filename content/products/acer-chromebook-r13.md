---
date: "2018-05-15T18:53:23Z"
weight: 2

id: '2'
mfg: 'Acer'
family: 'Chromebook'
price: '999'
name: 'Chromebook R 13'
serie: 'CB5-312T-K4FT'
cover_image: 'https://static.acer.com/up/Resource/Acer/Laptops/Chromebook_R13/Images/20160824/Chromebook-R-13_main.png'
images:
    - https://static.acer.com/up/Resource/Acer/Laptops/Chromebook_R13/Photogallery/20160824/Chromebook-R13_gallery_01.png
    - https://static.acer.com/up/Resource/Acer/Laptops/Chromebook_R13/Photogallery/20160824/Chromebook-R13_gallery_02.png
    - https://static.acer.com/up/Resource/Acer/Laptops/Chromebook_R13/Photogallery/20160824/Chromebook-R13_gallery_03.png
desc_link: 'https://www.acer.com/ac/en/US/content/series/acerchromebookr13'
specs_link: 'https://www.acer.com/ac/en/US/content/model/NX.GL4AA.003'
play_support: 'Stable'
summary: '13.3'' Touchscreen, 4 GB RAM, 64 GB,  MediaTek CPU'
display: '13.3" Full HD (1920 x 1080) resolution multi-touch IPS technology'
display_angle: '360'
display_touch: ''
stylus: 'NO'
ram: 'LPDDR3 4 GB (standard)'
cpu: 'MediaTek M8173C 2.1 GHz; Quad-core'
graphic: 'Imagination Technologies PowerVR GX6250 LPDDR3 Shared graphics memory'
storage: '64 GB flash drive'
power_supply: 'USB-C'
interface: '1x USB 3.0 HDMI Output 1x USB 3.1 Type-C'
battery: '3-cell 4670 mAh Li-Polymer'
dimensions: '325 x 228 x 15.5'
device_weight: '1.5kg'
options:
    - playstore
    - stylus
    - rugged
---