+++
title = "Getting started with using Google Drive the right way"
type = "lab"
layout = "article"
draft = false
date = "2017-10-17T10:00:00Z"
lastmod = "2017-10-17T10:00:00Z"
author = "Pascal Aubort"
imageCredit = "Pascal Aubort"

slug = "getting-started-google-drive-the-right-way"

aliases = [
    "/lab/2017/10/getting-started-with-using-google-drive-the-right-way/"
]

headline = "How to get started with Google Drive the right way"

meta = "Need to ensure that all your staff or partners have the most up-to-date version of company figures for that crucial sales pitch? Get started with Google Drive and store your files in the cloud."


categories = [ "Google Drive" ]
tags = [ "Google Drive", "Getting Started"]



[coverImage]
    url = "05-00-cover-900_502-min.jpg"
    width = "900"
    height = "502"
+++

Need to ensure that all your staff or partners have the most up-to-date version of company figures for that crucial sales pitch? Need to update your files on the go from a mobile device? Storing files in the cloud could be the solution you need, and is why it is becoming increasingly popular for businesses and private users. This article provides an overview of cloud storage with Google Drive.

## What is Google Drive anyway?

Google Drive is an __online file storage with advanced collaboration capabilities__. Essentially, you can see this as having your own hard drive located in one of Google’s secure data centers, accessible from anywhere at any time. There is a multitude of services that provide online cloud storage similar to Google Drive and you might have heard of names like Dropbox, Box, etc. 

This article will not seek to compare these services but rather is about understanding how to successfully get started with Google Drive. In order to access Google Drive, you need a Google Account. Google Accounts can be setup for free when you create a __Gmail account__ or via a paid subscription for __G Suite for Businesses__. 

>#### Google Drive or Google Docs?  
You have heard about Google Docs and are wondering what the difference between Docs and Drive is? Google Docs (Text editor), similar to Google Sheets (Spreadsheet editor) and Google Slides (Presentations editor) are applications that allow for online collaboration. These files are stored in Google Drive.
Think about it like computer programs that allow you to create certain types of files (Word, Excel, PowerPoint) and stores them onto your computer, except that this is all done in the cloud

## Sounds good, but is it secure?

Your data is stored online. What about security then? Google uses the highest security standards when it comes to physical security in their datacenters as well as full encryption of the stored data. So, all of Google’s expertise is used to keep your data safe.
More about this on [Google’s Security website](https://privacy.google.com/your-security.html).

### What about users and security? 
That depends on how much effort you put into securing your Google Account. Google, like many other online services, provide what is called __“2-Step Verification”__ on all Google Accounts which requires you to type in a 1-time code - similar to your e-banking system - when you sign-in into your account from any device. This extra authentication step ensures that even if someone has discovered your password, they won’t be able to log into your account without this second security code. The [Security Checkup page](https://myaccount.google.com/secureaccount) of your Google Account will help you setup 2-Step Verification.


## What type of files can I store?
Now that we know what Google Drive is and that we have made sure our Google Account is secure, let’s dive into what we can do with Google Drive! As stated before, Drive is a cloud file storage system. In it, pretty much any type of file of up to 5 TB (!) in size can be stored, from Text to Video and photo files, from Photoshop to PDF. See this [help section](https://support.google.com/drive/answer/37603?hl=en) for a complete list of viewable files and file sizes. 


## How much does it cost?
The most common way to use drive is to have a __free Gmail account__, with which you have 15 GB of free storage. Once you reach the limit of 15 GB, you can either decide to do some spring cleaning, or buy additional storage for a few Francs a month. See the [Google Drive Pricing](https://www.google.com/drive/pricing/) page for more details.

Another popular way to use Google Drive is to purchase a [G Suite](https://gsuite.google.com/) account for your company. __G Suite (formerly known as Google Apps for Work)__ not only gives you access to Drive, but also to Gmail, Calendar, and all productivity apps from Google, __starting at 5.- Fr. per month__ per user for 30GB Drive Storage. Other plans are also available for companies that need unlimited Drive storage. 

> Remember that files created with Google Applications (Sheets, Docs, etc) __do not count towards your data quota__. That means you can save an unlimited amount of Google files into your drive without worrying about storage capacity.


## Using Google Drive from your Browser

Ready? Perfect, now open a new browser tab and go to [https://drive.google.com](https://drive.google.com). You will be taken to your Google Drive home page. Notice something? There are folders on the left sidebar, files in the main section of the page, something that all of us are used to. This makes for a seamless transition to using Google Drive.

{{% amp-img image="05-01-1200_599-min.png" width="1200" height="599"
    caption="All your data stored in the cloud are accessible via the Google Drive webapp"
%}}


1. Sidebar: This is where you will find the folder and subfolder structure, similar to the Files explorer on your PC or the Finder on you Mac. 
1. In the main section, files and folders are listed and can be opened by simply double-clicking on them.
1. The Quick Access section leverages Artificial Intelligence in order to suggest files that you might be interested in right now.
1. Search for documents using the search functionality
1. Access settings, documents details and change the view of your drive files listing
1. Create a new Google Docs, Sheets, Slides file or a new folder. You can also use your mouse’s right-click to create new files and folders

When setting up Google Drive for the first time, I like to recommend some default settings. Using the cog-wheel Icon (5), open the Settings panel and set the following parameters. 

{{% amp-img image="05-02-900_514-min.png" width="900" height="514" 
    caption="The Google Drive settings allows you to enable conversion of Microsoft files and use Google Drive Offline"
%}}

+ Check __Convert Uploads__ so that Microsoft files uploaded to drive are automatically converted to Google Docs, Sheets, Slides format and can be fully edited afterwards.
+ __Offline__ allows you to access and edit Google Drive files even without an internet connection. Once you are back online, it will automatically sync your changes.

Feel free to take a look at the other settings in the __General__ tab and the __Notifications__ tab as we won’t cover this in details in this article.


## Create your first Google Doc
From your Google Drive, press __NEW > Folder__. Name your folder as you want and save. You should now see your new folder in the main section of your drive. Double-click on the folder and create a new file using your mouse's __right-click > Google Docs__.

A new Tab will open with a blank Google Docs file. Name your file on the top-left and start working on your document. Once done, close the browser tab.


Did you realise I didn’t ask you to save anything? Well, that’s because the Google Apps save your work automatically. This also means you can start working at home on your computer and then get on the Tram and continue editing your Google Docs or Sheet from your phone for example, without ever needing to carry USB drives or anything like that.


Uploading a file to Google Drive is also as easy as dragging and dropping one or multiple files from your computer to your Drive open in the browser. If you have selected the option to convert uploaded files to Google formats, your Excel, Word and PowerPoint files will automatically be converted to Docs, Sheets and Slides format.

Of course, you can also upload a file or a folder using the __NEW__ button. 

{{% amp-img image="05-03-1208_530.gif" width="1208" height="530" 
    caption="Easily add any files to Google Drive using a simple drag-and-drop"
%}}


## Continue with sharing your document

Do you remember the last time you created slides presentation as a team? Think about how tedious it was to keep track of all the changes and to send updated files per email. With Google Drive, you can forget all of that. Because everything is online, there is no more need to send attachments and updates to work on a document. With Drive, you can work with up to __50 collaborators__ at the __same time__ on the __same document__.

{{% amp-img image="05-04-950_445.gif" width="950" height="445" 
    caption="Collaborate in real-time on the same document with your colleagues"
%}}


In your Docs, Slides or Sheets document, click on the Share button at the top right corner, add your colleagues’ email addresses and press __Send__ to start collaborating on your document. It doesn’t matter whether they are on a computer, iOS or Android device since editing online documents works across all devices.


## Using the desktop clients

Because for some people, it is easier to use the file explorer of their __PC or Mac__, Google also provides desktop clients for PC and Mac as a way to synchronize your files from your computer without the need to upload files manually. This programme is called __Google Drive Backup and Sync__ for free gmail accounts and  __Drive File Stream__ for Business customers. Both versions are available for download from [Google’s download page](https://www.google.com/drive/download/). 

Once you have installed the Drive client, connect your Google Account and setup your local Drive folder.

After successful login, the files stored on your Google Drive will start synchronizing to your computer. You can browse this Drive folder just like any other folder on your computer. All the files that you add into the Drive folder will be automatically synchronized to your Google Drive online storage. 

{{% amp-img image="05-05-750_271-min.png" width="750" height="271" caption="Not ready to let go of your computer's file explorer? No problem, use the Google Drive app on Mac and Windows computers"

%}}

## Ready to take the step?

We have just scratched the surface of the wide-ranging possibilities that Google Drive offers in terms of productivity, collaboration and security. This article is intended to help you make your first steps into the Cloud collaboration world. Here is a list of resources that you can use to make the next step and find more information. 

+ [Google Drive Help Section](https://support.google.com/drive/?hl=en#topic=14940)
+ [Pricing Plans](https://www.google.com/drive/pricing/)
+ [G Suite website](https://gsuite.google.com/)

This post was sponsored by neustarter.ch, read the [German version here](https://neustarter.com/magazine/google-drive-richtig-verwenden?utm_source=loyall&utm_medium=lab&utm_campaign=google_drive).

<!--About the Author-->
<!--Pascal Aubort is an expert in G Suite, Google’s productivity tools. He is a former Google employee and is Founder of [loyall.ch](https://loyall.ch) which mission is to help companies and schools make the transition to the Cloud.-->








