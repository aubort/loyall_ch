+++
title = "Automatically build and deploy a site on App Engine"
type = "lab"
layout = "article"
draft = false
date = "2017-05-01T10:00:00Z"
lastmod = "2018-05-01T10:00:00Z"
author = "Pascal Aubort"

headline = "Build and deploy your website on Google App Engine using a script"

meta = "Make deploying your site to Google App Engine easy and less error prone using a sript. Learn How!"

slug = "automatically-build-and-deploy-a-site-on-app-engine"

aliases = [
    "/lab/2017/05/automatically-build-and-deploy-a-site-on-app-engine/"
]

categories = [ "how to" ]
tags = [ "hugo", "deploy", "script", "app engine" ]

[coverImage]
    url = "03-deploy-cover.jpg"
    width = "560"
    height = "315"
+++

In a previous article, I explained [how to set up a static site using Hugo, Cloud 9 and App Engine]({{< relref "001--2017-01--c9HugoGAE.md" >}}). In this article I describe how to make it easier to deploy the site to Google App Engine using a script. 
<!--more-->

## Create the script file

This script will make the deployment, whether on a staging version or on the production server, more efficient and less error prone. In order to get started, you will need to create a new file and make it executable straight away.

``` bash
$ touch ./deploy && chmod +x ./deploy
``` 

Now you should see the newly created `deploy` file at the root of your workspace. The next step is to open this file and paste the content of the script below, save and close.

``` bash
#!/bin/bash

# By default, we do not promote the deployed version. 
#This would allow all traffic to be routed to the new version. 
promote="--no-promote"

# Default version is empty, which means gcloud util will assign a new 
# version automatically
version=''

# Build Drafts flag. By default empty, so that drafts are not included
build_drafts=''

# The default commit message when deploying a new release
message="rebuilding site `date`"

# gcloud deploy command
command="gcloud app deploy"

################################################################################
# Display the help section 
################################################################################
display_help() {
    echo "Usage: $0 [option...]" >&2
    echo
    echo "   -v, --version              Set the App Version in the appengine console. If no version is given, App Engine will automatically create a random version"
    echo "                              Usage: --version=staging"
    echo "   -m, --message              Set a custom commit message for the deployment. If no argument is given, the current date is used as commit message"
    echo "                              Usage: --message=\"Building new site Version 1.0\""
    echo "   --promote                  By default the script does not promote new deployment. To route traffic to the newly deployed version add the --promote flag"
    echo "                              A version can also be promoted after deployment via the App Engine console (https://console.cloud.google.com/appengine)"
    echo "   --buildDrafts              Tells Hugo to build drafts. Default is false. "
    echo
    echo
    exit 1
}

# Parse script arguments 
for i in "$@"
do
case $i in
    -v=*|--version=*)
    version="--version=${i#*=}"
    ;;
    -m=*|--message=*)
    message="${i#*=}"
    ;;
    --promote)
    promote="--promote"
    ;;
    --buildDrafts)
    build_drafts="--buildDrafts"
    ;;
    --help)
    display_help
    ;;
    *)
    ;;
esac
done

echo -e "\033[0;32mDeploying updates to Google App Engine...\033[0m"

# Delete public dir
rm -rf public

#Build the sass files ->>> REMOVE THIS IF YOU DON'T USE GULP
gulp build

# Build the project.
hugo $build_drafts

# Add changes to git.
git add -A

# Commit changes.
git commit -m "$message"

if [ -z "$version" ]; then
  version="--version=$(git rev-parse --short HEAD)"
fi

# Push source and build repos.
git push google master

# Build the command line with the arguments 
command="$command $version $promote"

#Print the command
echo $command

# Run the deployment command
eval $command
```

__IMPORTANT__: If you are not using Gulp as a processor for your CSS Files, you need to remove the line with `gulp build`!

## Use the script

Before you are able to use the script to deploy anything to Google App Engine, you need to make sure your `gcloud` SDK is properly setup. I have explained the steps in [this previous post]({{< relref "001--2017-01--c9HugoGAE.md" >}}).

Now that you are all set, take a look at the help function included with the script.

``` bash
$ ./deploy --help
```

Here are a few examples of commands that can be useful

### Add a custom commit message
You can define a custom commit message when building the site. By default there is a message with the current timestamp.

``` bash
$ ./deploy -m="Rebuilt the site for version 0.1"
``` 
### Define a version number
By default, the scripts sets the App Engine version of your deployment to the value of the git hash. That makes it easy to track which version of your code is deployed. You can however set the version manually. 

``` bash
$ ./deploy --version=staging
``` 

### Force `promote` (Danger zone)
By default the script does not promote (does not redirect the traffic to the deployed version). If you are sure about what you are doing, you can force promote the deployment so that all the traffic is redirected to the new version.

``` bash
$ ./deploy --promote
``` 

### Combine flags
Of course, you can combine flags such as

``` bash
$ ./deploy --version=production --promote -m="Yes, I am sure I will not break anything"
``` 

That's it!