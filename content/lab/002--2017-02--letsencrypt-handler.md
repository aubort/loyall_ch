+++
title = "Let's Encrypt certificates without redeploying your app"
type = "lab"
layout = "article"
draft = false
date = "2017-02-08T10:00:00Z"
lastmod = "2017-09-22T10:00:00Z"
author = "Pascal Aubort"

slug = "lets-encrypt-certificates-without-redeploying-your-app"

aliases = [
    "/lab/2017/02/lets-encrypt-certificates-without-redeploying-your-app/"
]

headline = "Add SSL Certificates to your static website without redeploying on App Engine"

meta = "This tutorial describes how to add SSL Certificates to your website hosted on Google App Engine without redeploying, using GAE Memcache."


categories = [ "how to" ]
tags = [ "app engine", "SSL", "lets encrypt" ]

[coverImage]
    url = "02-letsencrypt-01.jpeg"
    width = "560"
    height = "315"
+++
  
   
>#### Update 1 - 22 Sep 2017
On September 15, 2017, Google
[announced](https://cloudplatform.googleblog.com/2017/09/introducing-managed-SSL-for-Google-App-Engine.html) the introduction of managed SSL Certificates on Google App Engine. Head to the [post](https://cloudplatform.googleblog.com/2017/09/introducing-managed-SSL-for-Google-App-Engine.html) to get started. 


A few months ago, [Google announced](https://security.googleblog.com/2016/09/moving-towards-more-secure-web.html) their plan to make the web more secure. That means the next version of Chrome (56) will display more obviously whether a site is using a secure HTTPS connection in the address bar like below.
<!--more-->

{{% amp-img image="02-letsencrypt-02-1.jpeg" width="560" height="174" %}}

That also means it is now time to make your site and your visitors secure using HTTPS. When we started building our site, it was clear that we needed an "easy" way to make it secure and at the lowest cost possible. After looking at the many commercial options out there, we discovered [Let's Encrypt](https://letsencrypt.org/), an **open** and **free** Certificate Authority. Our site is hosted on Google App Engine (go runtime) and it turned out that Let's Encrypt certificates are supported by GAE. 


In this article I will describe how to make the certificate creation and renewal as easy as possible with a custom handler. There are plenty of articles out there, in particular [this one](https://www.m0d3rnc0ad.com/post/letencrypt-setup/) which describes very well all the steps to create the SSL Certificates and add them to App Engine.

Because Lets' Encrypt are only valid for 90 days, after which they have to be renewed, I decided that I would write a custom handler in Go to deal with the `/.well-known/acme-challenge` without having to deploy our site multiple times everytime the certificates have to be renewed.

In order to achieve this, I am using the [App Engine Memcache](https://cloud.google.com/appengine/docs/go/memcache/) to store the challenge temporarily and a custom Go handler to retrieve the values from the memcache. 

Basically, when creating your certificate, for the naked domain and each sub-domain, you will be prompted a URL and a challenge, similar to this.

``` bash
Make sure your web server displays the following content at
http://domain.com/.well-known/acme-challenge/k_F9xhVZjCmavFRePY0qRnPQTP2Wog_K6szbPWUS1sQ before continuing:

k_F9xhVZjCmavFRePY0qRnPQTP2Wog_K6szbPWUS1sQ.UfRTCl-YJkBwds1kza7iK2_IDQMjzIsYKmsPTMeqAgQ

```

## The Go Handler

In your `main.go` file, add the following custom handler. This handler will get the part after `/acme-challenge/`, search for it in the memcache and return the corresponding challenge.

``` go 
func LetsEncryptHandler(w http.ResponseWriter, r *http.Request) {

  ctx := appengine.NewContext(r)

  key := strings.Split(r.URL.Path, "/")[3]
	ctx.Infof("%s", key)

  w.Header().Set("Content-Type", "text/plain; charset=utf-8")

  if item, err := memcache.Get(ctx, key); err == memcache.ErrCacheMiss {
  	ctx.Infof("item not in the cache")
  } else if err != nil {
  	ctx.Errorf("error getting item: %v", err)
  } else {
  	ctx.Infof("the challenge is %q", item.Value)
  	challenge := string(item.Value)
  	w.Write([]byte(challenge))
  }
}
```

Now, in the `main.go` file, add the following line to the `init()` function.

``` go
func init() {
  
  ...
	http.HandleFunc("/.well-known/", LetsEncryptHandler)
  ...
}
```
Make sure to add the following handler in the `app.yaml` too:

``` yaml
- url: /.well-known/.*
  script: _go_app

- url: /.*
  script: _go_app
  secure: always
```

## Getting the Challenge in Memcache

Once you have done that and deployed your app, you can start creating your certificate and adding the challenges and keys to App Engine's Memcache.

First go to your [App Engine Console] (https://console.cloud.google.com/appengine/memcache), in the Memcache section. Then press the "New Key" button to get the the new key screen.

{{% amp-img image="02-letsencrypt-03.jpg" width="400" height="179" %}}


Now create a new key using `Go String` as the Key type and `String` as Key value. You can now copy the key and challenge from your terminal, similar to what is displayed in this example. The `Key` field being the part after the `/acme-challenge/` in the URL.

{{% amp-img image="02-letsencrypt-04.jpg" width="400" height="309" %}}


Once you have done so, press the Create value button and you are set. In order to verify that this is working as intended, copy and paste the acme-challenge URL (http://domain.com/.well-known/acme-challenge/k_F9xhVZjCmavFRePY0qRnPQTP2Wog_K6szbPWUS1sQ) in your browser and verify that the challenge is being printed out. 

You can now repeat the above steps for all your keys and challenges for all your subdomains and [continute the certificate creation and verification](https://www.m0d3rnc0ad.com/post/letencrypt-setup/#hosting-page-to-verify-site). 

