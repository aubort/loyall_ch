+++
title = "G Suite: An introduction to getting started working in the Cloud"
type = "lab"
is_amp = false
layout = "article"
draft = false
date = "2018-03-21T10:00:00Z"
lastmod = "2018-06-16T10:00:00Z"
author = "Simon Parker"
imageCredit = "Pascal Aubort"

slug = "g-suite-introduction-get-started-cloud"


headline = "G Suite: An introduction to getting started working in the Cloud"

meta = "Discover how G Suite (formerly Google Apps) can help your team be productive and collaborative from day one."


categories = [ "G Suite" ]
tags = [ "Google Apps", "G Suite" ]


[coverImage]
    url = "06-gsuite-cover-900_540-min.jpeg"
    width = "900"
    height = "540"
+++

Your idea is ready, you’re convinced of its potential and you’re ready to build a team and start planning a launch. And now you face the decision of what tools are you going to use to communicate your ideas, to stay in contact with your clients, to collaborate with your partners, and to manage your meetings. What about storing all your data? And of course, how can you meet all these needs within your budget?

As a new business, you’ll find that G Suite (formerly Google Apps) provides you with a set of tools to start your business and manage it as it grows at a reasonable cost. A calendar, email service, a tool to communicate live with your partners and customers, spreadsheets for budgets, orders and analyses of data, software to create engaging presentations, and many more tools. And all of these based in the cloud, behind a strong security wall, and accessible anywhere at anytime.

Thanks to the functionality embedded in each of these tools, your business partners can all collaborate live on the same documents. Oh, and you’ll get unlimited data storage with Google Drive, so you’ll never have to worry about “upgrades” to your servers as your business grows.

**See also:** [How to get started with Google Drive the right way](https://loyall.ch/lab/getting-started-google-drive-the-right-way/)

These benefits are the reason that [more than 4 million companies](https://www.techradar.com/news/g-suite-is-powering-ahead-with-4-million-customers) around the world are already using G Suite, from large corporations to startups. Public and private schools here in Switzerland and in dozens of other countries use G Suite for Education and the numbers are growing daily.

Many individuals already use many of the G Suite tools because they come with a simple  gmail account but subscribers to G Suite for Business gain many added features. For example, email accounts with your company domain name and logo. Or, easily manage users and the settings of all applications from a single admin console. Single sign-ons for countless other applications means that you can easily integrate other tools into your G Suite environment.

With all these features being based in the cloud, your data is always accessible (you can even work offline and sync later if you have to). There is no need to worry about the security of this data though because [over 700 security engineers](https://www.google.com/intl/en_ch/cloud/security/) work at Google to keep your data safe on a daily basis. Ensuring the security of your users and their devices can be enhanced by activating mandatory two-step authentication for Chromebooks, Android and iOS phones and tablets.

{{% amp-img 
    image="06-01-gsuite-2factor-778_483-min.jpg" 
    width="778" height="483" class="" 
    caption="Enabling 2-step Verification is the best way to keep your data safe and can be enabled for all users in your domain using the G Suite admin console."%}}

Free versions of G Suite are available for non-profits and educational institutions, and businesses pay as little as CHF 5.- per user per month.

As the data in our networks continue to increase, it becomes a challenge to locate that email, report or spreadsheet you urgently need because it is buried in some inappropriately named folder. However, Google’s powerful search tool integrates Artificial Intelligence to comb through the darkest corners of your filing system to locate what you need, even if you can’t remember exactly what that file was called.

AI is also embedded in many of the G Suite tools to help you get things done, such as suggesting charts or formulas in spreadsheets to analyse data, suggesting meeting times for you and your partners based on your individual calendars, or suggesting designs for your presentations.

G Suite also makes real-time collaboration among colleagues possible on files such as documents or presentations. This means that you will never have to worry about ensuring that everyone has the latest version of the sales figures, or is working on the same version of the pitch to that prospective client.

In G Suite, everyone can work on the same file at the same time, regardless of where they are and all changes can be tracked and are saved automatically. Of course, your team can also work on the same document at different times, but with cloud-based computing you know that everyone has the most recent version of every file.

{{% amp-img image="06-02-gsuite-collaboration-1000_568-min.png" width="1000" height="568" class=""
caption="Real-time collaboration enables all your team members to work on the same file at the same time, on any device."
%}}

Another benefit of using G Suite is that all the numerous Google products integrate seamlessly with one another because they “talk” to each other using their APIs and add-ons. This is efficient and economical for you.

On the G Suite Marketplace you’ll also find hundreds of other integrated applications, as CRM, Customer support platforms, School information systems, and backup systems, so you can truly customise your digital tools to your specific needs.

As a dynamic and innovative company, Google [launches new features and improvements](https://gsuite.google.com/whatsnew/) to existing tools almost weekly so you can always be at the cutting edge of digital solutions for your business.

{{% amp-img image="06-03-gsuite-marketplace-800_473-min.png" width="800" height="473" class=""
caption="G Suite integrates with hundreds of third-party applications such as CRMs, educational tools, invoicing and more, directly accessible from the G Suite Marketplace."%}}

Within the G Suite list of products, you have the ability to customise programmes to suit your own needs. For example, It is quite easy to create your own formulas in Sheets, which is Google’s spreadsheet programme, making it easy for you to set them up to meet your specific needs. Here is an example of how to [generate random Passwords in Google Sheets](https://loyall.ch/lab/email-and-passwords-with-google-sheets-custom-functions/).

Moving to G Suite is not a gargantuan task and is one you could manage yourself if you have an adventurous spirit. Ideally though, calling on the services of an expert - such as [Loyall](https://loyall.ch) - will ensure that the set up is painless for you, and you can get training for yourself and your team at the same time.

G Suite - give your business a head start.

This post was sponsored by neustarter.ch, read the [German version here](https://neustarter.com/magazine/g-suite-neustarter-in-der-cloud?utm_source=loyall&utm_medium=lab&utm_campaign=g-suite).
