---
date: "2017-07-23T11:46:20Z"
draft: false
title: "Wieso Loyall als Partner für Ihre Digitalisierung wählen"
meta: "Erfahren Sie, wie sich unser Vorgehen von dem anderer Anbieter abhebt!"

page_title: "Wieso Loyall wählen"
page_subtitle: "Moderne und ganzheitliche Cloud IT-Lösungen für professionelle, innovative Organisationen wie Ihre."


usps:
  - heading: Unsere Erfahrung
    copy: "Loyall GmbH ist ein Schweizer Cloud IT-Beratungsunternehmen mit Sitz in Zürich. Wir sind auf Cloud-Kommunikation und Organisations-Technologie für kleine und mittlere Unternehmen und Bildungsinstitutionen spezialisiert."
  
  - heading: Loyalität und Transparenz
    copy: "Unsere Mission ist es, Ihnen die beste und kostengünstigste Cloud IT-Lösung für Ihre aktuellen Bedürfnisse und längerfristigen Unternehmensziele zu bieten, ganz gleich in welcher Branche Sie arbeiten."

  - heading: Individueller Service
    copy: "Bei Loyall gibt es keine Standard-Lösungen für alle. Für Sie gestalten wir eine individuelle Lösung, welche optimal zu Ihrem Unternehmen passt. Wir sind stolz darauf, jeden Tag transparent und zielorientiert zu arbeiten und ehrlichen, wertvollen IT Support anzubieten."


segments:

  - heading: "Unser Angebot für Ihr Unternehmen"
    bg_image: "segment-company"
    copy: "Mit der Installation einer geteilten IT-Infrastruktur profitieren Unternehmen auf mehreren Ebenen. Dezentralisierte Unternehmen reduzieren die Notwendigkeit von Geschäftsreisen und können dennoch Face-to-Face Meetings anhand von Videokonferenzen durchführen. Verbessern Sie die Kommunikation sowohl mit Ihren Mitarbeitenden als auch mit Ihren Kunden, indem Sie Dokumente online teilen, diese in Echtzeit bearbeiten und die Zusammenarbeit vereinfachen."
  
  - heading: "Unser Angebot für Ihre Schule"
    bg_image: "segment-education"
    copy: "Zukünftige Generationen auszubilden ist eine grosse Herausforderung. Mit den besten IT-Lösungen, sind Sie für die Zukunft gewappnet. Wir bieten Ihnen die Integration von Cloud-Lösungen sowohl für den Unterricht als auch für die Administration. Zusätzlich bieten wir auch individuelle Schulungen an, welche die geschäftlichen wie auch die pädagogischen Aspekte Ihrer Institution abdecken."
  
  - heading: "Unser Angebot für Ihre NGO"
    bg_image: "segment-ngo"
    copy: "Sich um Volontäre, Spender und Ihre Wohltätigkeitsveranstaltungen zu kümmern und die Kommunikation auf allen Ebenen aufrecht zu erhalten, kann eine echte Herausforderung sein. Cloud-basierte Tools machen dies einfach indem sie all Ihre täglichen Programme in zugänglichen Plattformen vereinen. So wird Ihre ehrenhafte Arbeit von allen Seiten unterstützt."



solutions_title: "Wir finden die beste Cloud IT-Lösung für Ihr Unternehmen, Ihre Schule oder NGO"
solutions_intro: "Loyall empfiehlt nur die besten Technologien um sicherzustellen, dass Ihr Arbeitsalltag wie geplant verläuft und Ihre Kunden rundum umsorgt sind. Ganz gleich, wer Ihre Kunden sind oder wie Ihr Unternehmen geleitet wird: Unsere IT-Lösungen können Ihnen helfen, Ihre Produktivität und Leistung tagtäglich zu steigern."

  
services_title: "Stabiler und weitreichender IT-Service"
services_intro: "Durch unseren stabilen und umfassenden Cloud IT-Service unterstützt Loyall Sie dabei, Ihnen bleibenden Erfolg zu ermöglichen und Ihre täglichen Aufgaben zu vereinfachen."

services:
- heading: "Individuelle Analyse "
  copy: "Wir arbeiten direkt mit unseren Kunden zusammen, um spezifische Bedürfnisse und Ziele zu verstehen. Wir erkunden alle möglichen Optionen, damit wir die perfekte Lösung finden, welche ideal auf die individuellen Bedürfnisse unserer Kunden eingeht. Während dieser Phase definieren wir zudem den Umfang, den Zeitplan und die Meilensteine für eine erfolgreiche Umsetzung Ihrer Cloud IT-Lösung."

- heading: "Umsetzung"
  copy: "Die Umsetzungsphase beinhaltet den technischen Aufbau der ausgewählten Lösung sowie die Migration sämtlicher vorgängigen Systeme. Weil uns der menschliche Aspekt beim Einführen neuer Arbeitsmethoden extrem wichtig ist, kreieren wir zusammen mit unseren Kunden einen Change-Management Prozess, welcher über den ganzen Zeitraum der Umsetzung hinweg anhält."

- heading: "Schulung"
  copy: "Ihre technischen Mitarbeiter und Endnutzer werden von individuellen Schulungen und Gruppen-Workshops profitieren. Es ist wichtig, die richtige Schulung im optimalen Format anzubieten, um eine hohe Benutzer-Akzeptanz zu erzielen und Ihren technischen Mitarbeitern die Verwaltung der neuen Lösung zu ermöglichen." 

- heading: "Support"
  copy: "Wir bieten kostenfreien und unlimitierten Support während der Umsetzung an. Anschliessend leisten wir Support auf Abruf. Dies bedeutet, dass Sie gemäss dem Pay-per-Use-Modell bezahlen, ohne komplizierte Verträge unterschreiben oder Vorauszahlungen tätigen zu müssen."

# - heading: "Automatisierung"
#   copy: "Damit Sie sich auf die wirklich wichtigen Aufgaben konzentrieren können, helfen wir Ihnen Prozesse oder lästige und sich widerholende Aufgaben zu automatisieren. Wir helfen Ihnen Ihre aktuellen Prozesse zu analysieren und kreieren zusammen individuelle Skripts, welche Ihren Arbeitsalltag vereinfachen. Beispiele dafür sind die Erstellung und der Versand von Rechnungen anhand einer Tabelle, oder die Gestaltung einer professionellen E-Mail Kampagne mit Docs-Vorlagen."

# ci_title: "Kontinuierliche Anpassungen"
# ci_text: "Technologie verändert sich laufend. Deshalb werden wir Sie regelmässig über neue Funktionen und Tools informieren, die für Sie von Interesse sein könnten. Wir analysieren zum Beispiel die Benutzer-Akzeptanz oder bestehende Prozesse und bringen wo möglich Verbesserungsvorschläge an."



services_cta_title: "Sind Sie bereit in die Cloud zu wechseln?"
services_cta_intro: "Um Ihre individuellen Bedürfnisse zu verstehen kommen wir in einem ersten Schritt gerne zu Ihnen, und stellen Ihnen unsere Lösungen in einer unverbindlichen Präsentation vor."
services_cta_action: "Kontaktieren Sie uns jetzt"

agile: "Wir sind flexibel und agil und setzen auf ein anpassungsfähiges Projekt-Management für jeden einzelnen Kunden. Dies bedeutet, dass Sie während des Projekts Änderungen vornehmen können ohne die Umsetzung zu gefährden."

about_title: "Wer wir sind"


team_title: "Lernen Sie das Loyall Team kennen"
team_intro: ""

team:
  - name: "Pascal Aubort"
    title: "Gründer & Geschäftsführer"
    image: /assets/images/pascal-profile.jpg
    image_alt: "Profile photo of Pascal Aubort"
    linkedin: https://www.linkedin.com/in/aubort
    copy: "Nach meinem Studium als Media & Management Engineer habe ich in Afrika und Bulgarien bei Non-Profit Organisationen sehr positive Erfahrungen gemacht. Später habe ich bei <em>Google in Zürich</em> die Operations für Google Street View in Europa geführt und zuletzt als <em>Technischer Projektleiter</em> in einem erfolgreichen Startup in Zürich viele wertvolle Erfahrungen gesammelt. Dazwischen habe ich auf diversen - zum Teil auch längeren - Reisen viel erlebt und gelernt und bin überzeugt die notwendigen Tools und die Offenheit zu besitzen, um die Bedürfnisse unserer Kunden zu verstehen und diese durch unsere Dienstleistungen abzudecken. Meine guten <em>Französisch-, Deutsch- und Englischkenntnisse</em> tragen zur erfolgreichen Kommunikation bei. Ich freue mich sehr auf ein neues Abenteuer mit meiner Loyall GmbH, welche ich im September 2016 gegründet habe."

  - name: "Viola Schlumberger"
    title: "Partner Business Development"
    image: /assets/images/viola-profile.jpg
    image_alt: "Profile photo of Viola Schlumberger"
    linkedin: https://www.linkedin.com/in/viola7
    copy: "Ich begann 2005 bei Google und durfte erleben, wie das Unternehmen seither bis 2012 auf 1200+ Mitarbeiter angewachsen ist. Ich nahm die einzigartige Gelegenheit wahr, als Rekruterin <em>Google Schweiz</em> auf sämtlichen Ebenen mitzugestalten. Dabei absolvierte ich die Ausbildung zur Personalfachfrau und baute damit meine KV-Lehre mit BMS aus. Nach 6 Jahren bei Google habe ich mir eine längere Auszeit gegönnt und habe viele Länder bereist und meinen Horizont erweitert. Seither habe ich zwischen zwei längeren Reisen bei <em>Zürich Versicherung und Appway</em> in Zürich den Auf- und Ausbau bei der Rekrutierung unterstützt. Jetzt bin ich bei Loyall GmbH für Business Development, Marketing und die Administration mitverantwortlich."

---

