+++
date = "2017-12-30T11:46:20Z"
draft = false
title = "Contact Loyall | Get A quote for your Cloud today"
meta = "Have a question, need support, or would like a quote for your future cloud solution at your school, NGO or business? Contact us for prompt and friendly service."

page_title = "Let's connect!"
page_subtitle = "Via phone, email or form, choose the contact method most convenient to you."

+++