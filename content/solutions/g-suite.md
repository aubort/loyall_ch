---
date: "2017-09-04T11:46:20Z"
draft: false
layout: "g-suite"

title: "Ihr G Suite Partner | Loyall IT Lösungen"
meta: "Erfahren Sie, wie die Migration auf G Suite (vormals Google Apps) helfen kann, die Produktivität zu verbessern und Kosten für Ihr Unternehmen oder Ihre Schule zu senken."

page_title: "G Suite von Google Cloud"
page_subtitle: "Ihr G Suite Installations- und Migrationspartner für Unternehmen, Nonprofit-Organisationen und Schulen."

intro: "G Suite, vormals Google Apps for Work oder Google Apps for Education, ist ein Set von cloudbasierten Office- und Produktivitätstools. <br><br>Loyall ist Ihr Installations- und Migrationspartner für G Suite - die optimale Cloud IT Lösung für jedes Unternehmen, jede Schule und jede NGO."


features:

  - heading: "Erleichterte Zusammenarbeit"
    copy: "Ihr Team kann nun von jedem Ort aus zusammenarbeiten, ob online oder offline. Es kann von überall her und von jedem Gerät aus auf ein G Suite Konto zugegriffen werden."

  - heading: "Kostengünstig"
    copy: "Anstatt eines gesamten Paketes bezahlen Sie bei G Suite nur die Konten, die Sie aktuell nutzen. Zudem reduziert G Suite den zusätzlichen Bedarf an Verwaltungs-, Support- und Wartungspersonal."

  - heading: "Hohe Sicherheit"
    copy: "Schützen Sie Ihre Daten mit zuverlässigen Verschlüsselungs-Prozessen und zweifacher Authentifizierung. Mit G Suite müssen Sie sich nie den Kopf darüber zerbrechen, ob Ihre E-Mails oder Dokumente sicher sind."

  # - heading: "Hohe Sicherheit"
  #   copy: "Schützen Sie Ihre Daten mit zuverlässigen Verschlüsselungs-Prozessen und zweifacher Authentifizierung. Mit G Suite müssen Sie sich nie den Kopf darüber zerbrechen, ob Ihre E-Mails oder Dokumente sicher sind. Lesen Sie mehr über <a href='https://gsuite.google.com/security/' target='_blank' class='blank'>Google Datensicherheit</a>"

integrate: "Hunderte zusätzliche Applikationen sind auf dem G Suite Marketplace verfügbar"

trial: "Sind Sie bereit für Ihre kostenlose 14-tägige G Suite Testphase?"
trial_cta: "Kontaktieren Sie uns jetzt"


plans:

  - heading: "G Suite für Unternehmen"
    pricing: "<span class='f7 pt3'>Ab CHF</span><span class='f-headline lh-solid ph3 accent-color'>5</span><span class='f7 pt3'>pro Benutzer/Monat</span>"
    copy: "G Suite ist komplett browserbasiert und bietet ein breites Set an Tools an, welche Ihre tägliche Arbeit vereinfachen, wie zum Beispiel E-Mail, Kalender und Tabellen. Dabei behalten Sie Marken-Kontinuität über alle Plattformen hinweg durch professionelle E-Mails @IhrUnternehmen."
    link: "https://gsuite.google.com/intl/en_ie/pricing.html"
    features:
      - "E-Mail via Gmail @IhreDomain"
      - "Freigabefähiger Kalender"
      - "Online-Zusammenarbeit mit Docs, Tabellen und Präsentationen"
      - "Voice- und Videokonferenzen mit Hangouts"
      - "Web Foren und geteilte Inbox: Gruppen für Unternehmen"
      - "24/7 Telefon-, E-Mail- und Online-Support"
      - "Sicherheit- und Admin-Konsole"
      - "Mobilgerätverwaltung"
      - "Zugriff auf <a href='https://gsuite.google.com/marketplace/?hl=de' target='_blank'>G Suite Marketplace</a>"
      - "Integriert mit <a href='/solutions/chromebooks/'>Chromebooks</a>"
      - "30 GB Online-Speicher mit Drive"
    
  - heading: "G Suite for Education"
    pricing: "<span class='f7 pt3'>CHF</span><span class='f-headline lh-solid ph3 accent-color'>0</span><span class='f7 pt3'>pro Monat</span>"
    copy: "G Suite ist gratis für Bildungsinstitutionen und bietet beispiellose Kommunikations- und Produktivitäts-Tools, welche einfach in das Klassenzimmer integriert werden können. So können Lehrpersonen nun besser zusammenarbeiten und die Kommunikation mit Eltern wird vereinfacht." 
    link: "https://edu.google.com/products/productivity-tools/"
    features: 
      - "<i>G Suite für Unternehmen plus:</i>"
      - "Unlimitierter Drive Speicher (oder 1TB bei weniger als 5 Benutzern)"
      - "<a href='https://www.youtube.com/watch?v=GIN-EtPa0lw' target='_blank'>Google Classroom</a>"
      - "Einzigartige Gruppen & Programme für Pädagogen"
      - "Verbindung mit anderen Pädagogen"

  - heading: "G Suite für Nonprofit"
    pricing: "<span class='f7 pt3'>CHF</span><span class='f-headline lh-solid ph3 accent-color'>0</span><span class='f7 pt3'>pro Monat</span>"
    copy: "Verwalten Sie jeden Aspekt Ihrer Nonprofit- oder Wohltätigkeitsorganisation über eine einzige Oberfläche mit G Suite für NGO’s. Ermöglichen Sie Ihrem Team, einfacher mit Volontären, Spendern und Eventpersonal zu kommunizieren, damit jede Benefizveranstaltung und das Alltagsgeschäft einwandfrei abgewickelt werden können."
    link:  "https://www.google.com/nonprofits/products/apps-for-nonprofits.html"
    features: 
      - "<i>G Suite für Unternehmen plus:</i>"
      - "30 GB Online-Speicher mit Drive"
      - "Zugriff auf <a href='https://www.google.com/nonprofits/products/youtube-nonprofit-program.html' target='_blank'>YouTube NGO Programm</a>"
      - "Zugriff auf <a href='https://www.google.com/nonprofits/products/google-ad-grants.html' target='_blank'>Google Ad Grants</a>"
      - "Zugriff auf<a href='https://www.google.com/nonprofits/products/google-one-today.html' target='_blank'>Google One Today</a>"
      - "Zugriff auf <a href='https://www.google.com/nonprofits/products/google-earth-outreach.html' target='_blank'>Google Earth Outreach</a>"



why_loyall_title: "Warum sollten Sie Loyall für Ihre G Suite Installation wählen?"
why_loyall_intro: "Der Wechsel auf G Suite ist ein wichtiger Schritt für Ihr Unternehmen, ganz gleich wie gross Ihr Geschäft ist oder in welcher Branche Sie tätig sind. G Suite hilft Ihnen, im Geschäftsalltag schnell zu reagieren und Ihre Organisation und Produktivität auf zukünftiges Wachstum vorzubereiten. Loyall hilft Ihnen sowohl in technischer als auch in menschlicher Hinsicht mit dem Umstieg auf G Suite."

change_title: "Change Management"
change_text: "Um den Nutzungsgrad von G Suite und G Suite for Education zu maximieren, helfen wir Ihnen bei der Kommunikationsstrategie vor, während und nach der Installation."

tech_title: "Technische Umsetzung"
tech_text: "Als Google-zertifizierte Administratoren kümmern wir uns um das Domain-Management, die Nutzerversorgung, das E-Mail Routing, die Installation und die Migration von bestehenden Systemen."

train_title: "Massgeschneiderte Schulungen"
train_text: "Holen Sie das Beste aus G Suite und G Suite for Education mit unseren auf Ihre Zielgruppe zugeschnittenen Schulungen raus."

already_title: "Sie sind bereits ein G Suite Kunde?"
already_intro: "Das ist toll! Auch Sie können von unseren Dienstleistungen profitieren."

already_list: ["Maximieren Sie Ihre Datensicherheit indem Sie Ihr G Suite-Konto von einem Google-zertifizierten Administrator verifizieren lassen.",
"Steigern Sie Ihre Produktivität mit unseren massgeschneiderten Schulungen und unseren Best Practices.", "Führen Sie Ihre Schüler mittels Google Classroom in die Technologie ein."]

already_cta: "Kontaktieren Sie uns heute"
---