+++
date = "2017-07-10T11:46:20Z"
draft = false
layout = "office-365"

title = "Services d’implémentation d’Office 365 | Loyall solutions cloud"
meta = "Découvrez comment la puissante formule d’Office 365 peut améliorer l’efficacité de vos tâches quotidiennes et créer de la valeur ajoutée à votre entreprise ou votre école."

page_title = "Microsoft Office 365"
page_subtitle = "Vos outils Office favoris, plus de nouveaux outils dans une formule tout-en-un."

intro = "Microsoft Office existe depuis des décennies et est synonyme de courriers électroniques et agendas professionnels. Aujourd’hui, Office dispose de nouveaux atouts. Désormais, le Microsoft Office que vous connaissez et appréciez se nomme Office 365 et est disponible en logiciel de bureau et en application sur le Web. Accédez facilement aux documents Word ou aux feuilles de calcul Excel depuis n’importe où, avec une simple connexion à Internet."

tools_title = "Anciens outils, nouvelle accessibilité"
tools_text = "Tous les outils Office que vous connaissez et appréciez sont disponibles à la fois en application de bureau et dans le navigateur, rendant votre travail plus facile et plus accessible."

anywhere_title = "Disponible depuis n’importe où"
anywhere_text = "Désormais, tout ce dont vous avez besoin pour faire fonctionner votre activité est disponible à tout moment, en tout lieu. Il vous suffit d’une connexion Wi-Fi pour accéder à Office 365 et pouvoir effectuer vos tâches professionnelles."

sec_title = "Tranquillité d’esprit"
sec_text = "Conçu avec des dispositifs de haute sécurité intégrés, Office 365 vous permet de garder le contrôle, d’avoir une transparence sur vos données, et d'être en conformité avec les réglementations d’entreprise grâce aux contrôles de confidentialité intégrés."


business_link ="https://products.office.com/fr-fr/compare-all-microsoft-office-products?tab=2"

base_features_list = ["Versions en ligne de Outlook, Excel, Word et Powerpoint",
"Assistance web et téléphonique 24h/24 et 7j/7"]

business_features_title = "Office 365 pour les entreprises"
business_features_intro = "Profitez de la puissance d’Internet avec Office 365 pour les entreprises. Vous bénéficiez de tous les outils avec lesquels vous êtes familiarisé, plus de nombreux ajouts tels que OneDrive et OneNote, depuis le confort de votre bureau, de votre maison ou d’un hôtel pendant vos déplacements d’affaires."

business_1_features_pricing = "À partir de 8,50 CHF / Utilisateur / Mois"
business_1_features_list = ["1 To de stockage sur OneDrive", "Logiciel de bureau Office 2016 avec Word, Excel, Outlook, PowerPoint", "300 utilisateurs maximum"]

business_2_features_pricing = "À partir de 10.50 CHF / Utilisateur / Mois"
business_2_features_list = ["Hébergeur de messagerie @votredomaine", "SharePoint en ligne", "Vidéoconférence avec Skype pour entreprises", "Microsoft Teams, Yammer"]


edu_link = "https://products.office.com/fr/academic/compare-office-365-education-plans"

edu_features_title = "Office 365 Éducation"
edu_features_intro = "Bien qu’Office 365 soit idéal pour les entreprises, il représente également un atout majeur pour les enseignants et les étudiants. Non seulement vous êtes en mesure de gérer au mieux les aspects commerciaux d’un établissement d’enseignement, mais Office 365 Éducation permet également une collaboration sans équivalent entre enseignants et une excellente communication entre élèves, sur tout le campus."

edu_1_features_pricing = "Gratuit pour les étudiants et le personnel"
edu_1_features_list = ["Versions en ligne de Outlook, Excel, Word et Powerpoint", 
"Hébergeur de messagerie @votredomaine", 
"Bloc-notes pour la classe", 
"Stockage OneDrive illimité",
"Nombre d’utilisateurs illimité"]

edu_2_features_pricing = "À partir de 6 CHF / utilisateur / mois (étudiants) et 8 CHF pour les enseignants et le personnel"

edu_2_features_list = ["Conférence PSTN avec Skype", 
"Logiciel de bureau Office 2016 avec Word, Excel, Outlook, PowerPoint"]


ngo_link ="https://products.office.com/fr/nonprofit/office-365-nonprofit-plans-and-pricing"

ngo_features_title = "Office 365 pour les associations"
ngo_features_intro = "Faites davantage de bien dans le monde en gérant plus efficacement la communication et la productivité de votre organisation à but non lucratif. Office 365 aide les ONG du monde entier à rationaliser les communications internes et externes avec les donateurs, les bénévoles et les autres organisations."


ngo_1_features_pricing = "Don"
ngo_1_features_list = ["1 To de stockage sur OneDrive",
"Hébergeur de messagerie @votredomaine",
"Réunions en ligne illimitées",
"300 utilisateurs maximum"]

ngo_2_features_pricing = "À partir de 2 CHF / utilisateur / mois"
ngo_2_features_list = ["Logiciel de bureau Office 2016 avec Word, Excel, Outlook, PowerPoint", 
"Office sur tablettes et smartphones"]



why_loyall_title = "Pourquoi choisir Loyall pour votre intégration d'Office 365"
why_loyall_intro = "Chez Loyall, nous sommes persuadés qu’une entreprise organisée avec des canaux de communication fluides est mieux positionnée pour réussir. Partant de ce principe, nous faisons tous les efforts pour que votre migration et implémentation d’Office 365 se déroule sans encombre. Que vous migriez depuis une version obsolète d’Office ou depuis une plateforme totalement différente, nos experts techniques sont en mesure d’effectuer la transition de manière douce et précise."

change_title = "Aide au management"
change_text = "Maximiser le taux d’adoption d’une intégration réussie d’Office 365 est un élément clé. Nous vous aidons à élaborer des stratégies de communications avant, pendant et après votre transition."

tech_title = "Implémentation technique"
tech_text = "Nos administrateurs techniques prennent en charge les détails de base de votre intégration, incluant la migration des systèmes existants, le routage du courrier électronique, la configuration de l’acheminement, la gestion du domaine et le provisionnement des utilisateurs."

train_title = "Formations sur mesure"
train_text = "Nous prenons le temps d’aider votre équipe à se familiariser avec les bases d’Office 365, de sorte que vous puissiez tirer profit rapidement de votre nouvelle suite de communication."

cta_start = "Commencer dès aujourd’hui"

+++

