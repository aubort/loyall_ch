+++
date = "2017-04-18T11:46:20Z"
draft = false
layout = "office-365"

title = "Office 365 Implementierungs-Service | Loyall IT Lösungen"
meta = "Erfahren Sie, wie das einfach bedienbare und leistungsfähige Office 365 die Effizienz in Ihrem Arbeitsalltag erhöhen kann."

page_title = "Microsoft Office 365"
page_subtitle = "Ihre beliebtesten Office-Tools sowie neue Tools - Alle an einem Platz."

intro = "Microsoft Office gibt es bereits seit Jahrzehnten. Es ist ein fester Begriff für Unternehmens-E-Mail und Kalender. Heute hat Office noch mehr Tricks für Sie bereit. Das Microsoft Office, das Sie kennen und schätzen heisst nun Office 365 und ist als Desktop Applikation sowie als web-basierte Applikation verfügbar. Es gibt Ihnen einfachen Zugang zu Ihren Word Dokumenten oder Excel Tabellen von überall aus."

tools_title = "Altbewährte Tools, neues Zuhause"
tools_text = "Alle die von Ihnen geschätzten Office Tools sind über die Desktop Applikation sowie über Ihren Browser verfügbar. Dies macht Ihre Arbeit einfacher und zugänglicher."

anywhere_title = "Überall verfügbar"
anywhere_text = "Nun sind alle Dokumente, die Sie in Ihrem Arbeitsalltag brauchen, von überallher und zu jeder Zeit verfügbar. Alles was Sie brauchen ist eine Wi-Fi-Verbindung, um auf Office 365 zuzugreifen und Sie können sich sofort an die Arbeit machen."

sec_title = "Einen kühlen Kopf bewahren"
sec_text = "Office 365 ist mit hohen Sicherheitsoptionen ausgestattet und enthält Datenschutzkontrollen, mit welchen Ihre Daten und somit Ihre Geschäftsangelegenheiten in Sicherheit sind."


business_link ="https://products.office.com/en/compare-all-microsoft-office-products?tab=2"

base_features_list = ["Online-Version von Outlook, Excel, Word und Powerpoint",
"24/7 Telefon- und Online-Support"]

business_features_title = "Office 365 für Unternehmen"
business_features_intro = "Verhelfen Sie dem Potential des Internets mit Office 365 in Ihrem Unternehmen zum Durchbruch. Sie können von allen Tools, die Sie bereits kennen, profitieren und haben zusätzliche Tools wie OneDrive und OneNote zur Verfügung, welche Sie von Ihrem Büro aus, von Zuhause oder von unterwegs aus verwenden können."

business_1_features_pricing = "Ab 8.50 CHF/Benutzer/Monat"
business_1_features_list = ["1 TB Speicher mit OneDrive", "Office 2016 Desktop Applikationen mit Word, Excel, Outlook, PowerPoint", "Maximal 300 Benutzer"]

business_2_features_pricing = "Ab 12.50 CHF/Benutzer/Monat beinhaltet zusätzlich"
business_2_features_list = ["E-Mail-Hosting @Ihredomain", "SharePoint Online", "Videokonferenzen mit Skype for Business", "Microsoft Teams, Yammer"]


edu_link = "https://products.office.com/en/academic/compare-office-365-education-plans"

edu_features_title = "Office 365 for Education"
edu_features_intro = "Während Office 365 auf der einen Seite jedes Unternehmen bereichert, ist es in der heutigen Zeit der Technologie auf der anderen Seite ein Vorteil für Lehrpersonen und Studenten. Office 365 für Schulen ermöglicht es Ihnen Ihre Bildungsinstitution auf dem neusten Stand zu halten und fördert eine ausgezeichnete  Zusammenarbeit zwischen Lehrpersonen und eine einfache Kommunikation zwischen allen involvierten Parteien.."

edu_1_features_pricing = "Gratis für Studenten und Mitarbeiter"
edu_1_features_list = ["Online-Version von Outlook, Excel, Word und Powerpoint", 
"E-Mail-Hosting @Ihredomain", 
"Class Notebook", 
"Unlimitierter OneDrive Speicher",
"Unlimitierte Benutzerzahl"]

edu_2_features_pricing = "Ab 6 CHF Benutzer/Monat (Studenten) und 8 CHF für Mitarbeiter enthält zusätzlich"

edu_2_features_list = ["Skype Meeting Broadcast", 
"Office 2016 Desktop Applikationen mit Word, Excel, Outlook, PowerPoint"]


ngo_link ="https://products.office.com/en/nonprofit/office-365-nonprofit-plans-and-pricing"

ngo_features_title = "Office 365 für Nonprofit-Organisationen"
ngo_features_intro = "Konzentrieren Sie sich auf die wirklich wichtigen Ziele Ihrer Organisation indem Sie Ihre Kommunikation und Produktivität innerhalb Ihrer Nonprofit-Organisation verbessern. Office 365 hilft NRO’s weltweit, die interne und externe Kommunikation mit Spendern, Volontären und anderen Organisation zu optimieren."


ngo_1_features_pricing = "Spende"
ngo_1_features_list = ["1 TB Speicher mit OneDrive",
"E-Mail-Hosting @Ihredomain",
"Unlimitierte Online-Meetings",
"Maximal 300 Benutzer"]

ngo_2_features_pricing = "Ab 2 CHF/Benutzer/Monat"
ngo_2_features_list = ["Office 2016 Desktop Applikationen mit Word, Excel, Outlook, PowerPoint", 
"Office auf Tablets und Mobiltelefonen"]



why_loyall_title = "Wieso Loyall für die Installation von Office 365 wählen?"
why_loyall_intro = "Wir von Loyall glauben fest daran, dass ein gut organisiertes Unternehmen mit fliessenden Kommunikationskanälen zu mehr Erfolg führen kann. Deshalb arbeiten wir hart daran um sicherzustellen, dass Ihre Office 365 Migration und Anwendung zum einfachsten Teil Ihres Arbeitsalltages wird. Ob Sie eine veraltete Office Plattform zu Office 365 migrieren wollen oder von einer völlig anderen Plattform zu Office 365 wechseln wollen - unsere professionellen Technikexperten gestalten den Übergang reibungs- und schmerzlos."

change_title = "Change Management"
change_text = "Damit Sie  Office 365 von Anfang an optimal nutzen können helfen wir Ihnen auch mit der Kommunikationsstrategie vor, während und nach der Installation."

tech_title = "Technische Umsetzung"
tech_text = "Unsere technischen Administratoren kümmern sich um den Aufbau, das Domain-Management, die Nutzerversorgung, das E-Mail-Routing sowie  die Installation und die Migration von vorgängigen Systemen."

train_title = "Zugeschnittene Trainings"
train_text = "Wir nehmen uns Zeit, Ihrem Team die Basis von Office 365 vertraut zu machen, damit Sie das Beste aus Ihrer neuen Kommunikations-Suite herausholen können."

cta_start = "Starten Sie noch heute"
 
+++
 
