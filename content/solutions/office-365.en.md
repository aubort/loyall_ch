+++
date = "2017-04-18T11:46:20Z"
draft = false
layout = "office-365"

title = "Office 365 Implementation Services | Loyall IT Solutions"
meta = "Learn how the accessible and powerful addition of Office 365 to your business or school can increase the efficacy of your daily work and add overall value."

page_title = "Microsoft Office 365"
page_subtitle = "Your favorite Office tools, plus some new ones, all in one place."

intro = "Microsoft Office has been around for decades and is synonymous with business email and calendars. But, these days, Office has a few new tricks up its sleeve. Now, the Microsoft Office you know and love is called Office 365 and is available as both a desktop application and a web-based application. Easily access Word documents or Excel spreadsheets from anywhere with an internet connection."

tools_title = "Old tools, new home"
tools_text = "All of the Office tools you know and love are available in both the desktop application and in the browser, making your work easier and more accessible."

anywhere_title = "Available anywhere"
anywhere_text = "Now, everything you need to run your business and do the best job you can is available anytime, anywhere. As long as you have a Wi-Fi connection, you can access Office 365 and get work done."

sec_title = "Peace of mind"
sec_text = "Designed with high-security measures in place, Office 365 allows you control over your data, transparency, and business compliance with privacy controls built right in."


business_link ="https://products.office.com/en/compare-all-microsoft-office-products?tab=2"

base_features_list = ["Online version of Outlook, Excel, Word and Powerpoint",
"24/7 phone and web support"]

business_features_title = "Office 365 for Business"
business_features_intro = "Leverage the power of the internet for your business through Office 365 for small business. You can take advantage of all the tools you’re familiar with, plus great new additions like OneDrive and OneNote from the comfort of your office, your home, or a hotel while you’re on a business trip."

business_1_features_pricing = "From 8.5 CHF/User/Month"
business_1_features_list = ["1 TB Storage with OneDrive", "Office 2016 Desktop applications with Word, Excel, Outlook, PowerPoint", "Max 300 users"]

business_2_features_pricing = "From 12.5 CHF/User/Month includes in addition"
business_2_features_list = ["Email hosting @yourdomain", "SharePoint online", "Videoconferencing with Skype for Business", "Microsoft Teams, Yammer"]


edu_link = "https://products.office.com/en/academic/compare-office-365-education-plans"

edu_features_title = "Office 365 for Education"
edu_features_intro = "While Office 365 is great for businesses, it is an incredible asset for teachers and students in this technological age. Not only are you able to stay on top of the business aspects of running an educational institution, Office 365 for schools allows for unparalleled teacher collaboration and student communication across campus."

edu_1_features_pricing = "Free for Students and Staff"
edu_1_features_list = ["Online version of Outlook, Excel, Word and Powerpoint", 
"Email hosting @yourdomain", 
"Class notebook", 
"Unlimited OneDrive Storage",
"Unlimited Users"]

edu_2_features_pricing = "From 6 CHF user/month (students) and 8 CHF for staff includes in addition"

edu_2_features_list = ["Skype Meeting Broadcast", 
"Office 2016 Desktop applications with Word, Excel, Outlook, PowerPoint"]


ngo_link ="https://products.office.com/en/nonprofit/office-365-nonprofit-plans-and-pricing"

ngo_features_title = "Office 365 for Non-Profit"
ngo_features_intro = "Do more good in the world by better managing communication and productivity within your non-profit organization. Office 365 helps NGO’s worldwide streamline internal and external communications with donors, volunteers, and other organizations."


ngo_1_features_pricing = "Donation"
ngo_1_features_list = ["1 TB Storage with OneDrive",
"Email hosting @yourdomain",
"Unlimited Online Meetings",
"Max 300 users"]

ngo_2_features_pricing = "From 2 CHF/user/month"
ngo_2_features_list = ["Office 2016 Desktop applications with Word, Excel, Outlook, PowerPoint", 
"Office on Tablets and Phones"]



why_loyall_title = "Why choose Loyall for your Office 365 integration"
why_loyall_intro = "At Loyall, we believe that an organized business with fluid communication channels is better positioned for success. So, we work hard to make sure that your Office 365 migration and implementation is the easiest part of your day. Whether you’re migrating a dated Office platform to Office 365 or switching from an entirely different platform, our professional tech experts can make the transition smooth and painless."

change_title = "Change Management"
change_text = "Maximizing the user adoption rate of a successful Office 365 integration is key. We'll help you strategize basic communications before, during, and after your transition."

tech_title = "Technical Implementation"
tech_text = "Our technical administrators will take care of the back-end details of your integration, including legacy systems migration, e-mail routing, delivery setup, domain management, and user provisioning."

train_title = "Tailored trainings"
train_text = "We will take the time to help your team become acquainted or reacquainted with the basics of Office 365 to help you make the most of your new communication suite."

cta_start = "Get started today"

+++