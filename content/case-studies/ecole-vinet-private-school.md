---
date: "2017-09-11T11:46:20Z"
draft: false
type: "case"
layout: "case"
css_class: "vinet"
title: "Ecole Vinet Privatschule Fallstudie | G Suite Implementierung"
meta: "Kürzlich verhalfen wir einer etablierten Schweizer Privatschule dabei Technologie im Klassenzimmer zu integrieren und ihre IT-Kosten damit massiv zu senken."

bg_image: "case-vinet"
logo: "logo_vinet-min.png"

page_title: "Ecole Vinet Privatschule"
page_subtitle: "Implementierung von G Suite und Wi-Fi"

client: "Ecole Vinet ist eine etablierte Schweizer Privatschule, welche auf über 175 Jahre Erfahrung in der Ausbildung der klügsten Köpfe im Umfeld von Lausanne zurückblicken kann."

challenge: "<p>Ecole Vinet stand vor der Herausforderung einen praktischen, kosteneffektiven Weg zu finden, um moderne Technologie für Studenten und Lehrpersonen im Unterricht einzuführen.</p><p>Unsere Aufgabe war es eine kostengünstige und zuverlässige Lösung zu liefern, welche die Schule als Unternehmen zukünftig weiter wachsen lässt und dabei an die sich stetig ändernden Bedürfnisse des Unterrichts angepasst werden kann.</p>:"

solution: "Der erste Schritt für die Einrichtung einer geeigneten Technologie war die Installation eines neuen und zuverlässigen Wi-Fi-Netzwerkes auf dem gesamten Schulareal. Als nächstes ermöglichte die Implementierung von G Suite (vormals Google Apps for Education) den Lehrpersonen von Ecole Vinet eine neue Art der Zusammenarbeit. G Suite verfügt über eine einfach anzuwendende Plattform, die eine effizientere Verwaltung der Konten von Lehrpersonen innerhalb der Organisation ermöglicht. So konnten Kosten für externe Dienstleistungsanbieter drastisch gesenkt werden. Loyall unterstützt Ecole Vinet mit laufenden Maßnahmen in den folgenden Bereichen: <ul class='disk'><li>Durchführung von Workshops für Lehrpersonen und Verwaltungsmitarbeiter</li><li>Proaktive Kommunikation um sicherzustellen, dass die Schule von den neusten Funktionen profitieren kann.</li><li>Fortlaufende Prozessoptimierung und Unterstützung vor Ort als auch Remot-Support</li></ul>"

result: "Die Einführung der neuen Technologie und das optimierte Wi-Fi-Netzwerk führten dazu, dass der Schulalltag an der Ecole Vinet sowohl für Lehrpersonen als auch für Studenten moderner, einfacher und effizienter geworden ist."

testim_name: "Bernadette Kaba, Schulleiterin"
testim_text: "Das Loyall Team hat sofort verstanden was unsere Herausforderungen sind und hat uns perfekt passende Lösungen vorgeschlagen. Dank ihrer Zuverlässigkeit und ihrem Einsatz haben sie die Umsetzung der Einführung von Technologie in der Schule und im Klassenzimmer hervorragend gemeistert."

---