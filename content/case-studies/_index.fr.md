---
date: "2017-09-11T11:46:20Z"
draft: false
title: "Études de Cas | Les succès de Loyall"
meta: "Découvrez nos études de cas de cloud computing auprès d’entreprises et écoles, qui ont opté pour une solution informatique simple dans leurs usages quotidiens."

page_title: "Études de Cas"
page_subtitle: "Le succès de nos clients est un élément essentiel à notre mission et représente notre plus grande satisfaction. Pour vous en convaincre, découvrez quelques-unes de nos dernières études de cas clients et certaines de nos réussites."

cases: 
  - heading: "École privée Vinet"
    bg_image: "case-vinet"
    url: /case-studies/ecole-vinet-private-school/
    copy: "En tant que partenaire technologique, nous travaillons en continu avec l’école Vinet pour intégrer des technologies modernes et les outils <em>G Suite avec Google Classroom</em> au sein des salles de classe."

  - heading: "DMT Treuhand"
    bg_image: "case-dmt"
    url: /case-studies/dmt-treuhand/
    copy: "Nous avons aidé ce cabinet de conseil financier à migrer vers le cloud grâce à une solution hybride composée des formules G Suite et Office 365."

  - heading: "Galerie Plutschow"
    bg_image: "case-plutschow"
    url: /case-studies/plutschow-gallery/
    copy: "Cette galerie d’art a migré d’un serveur interne existant vers la suite logicielle G Suite afin de réduire les coûts et de travailler plus efficacement."

---