---
date: "2017-07-23T11:46:20Z"
draft: false
type: "case"
layout: "case"
css_class: "plutschow"

title: "Étude de cas Plutschow Gallery | Implémentation de G Suite"
meta: "Nous avons récemment aidé une galerie d’art à faire la transition d’une solution informatique tierce vers G Suite."

bg_image: "case-plutschow"
logo: "logo_plutschowgallery-min.png"


page_title: "Galerie d’art Plutschow"
page_subtitle: "Implémentation et migration vers G Suite pour une galerie d’art"

client: "La galerie Plutschow est une galerie d’art située à Zürich, en Suisse.<br>Comme beaucoup de PME suisses, la galerie Plutschow faisait appel à des intervenants externes pour tous leurs besoins en informatique, qu’il s’agisse de créer un nouveau compte de messagerie ou d’exécuter une sauvegarde de leurs données."

challenge: "Le propriétaire de la galerie, M. Plutschow, nous a contacté afin d’en apprendre d’avantage sur la formule de cloud G Suite et ses avantages pour son activité professionnelle. <br>Un challenge dans ce cas particulier consistait en l'intégration de leur système de gestion spécifique aux objets d'art."

solution: "Après avoir démontré à l’équipe le mode de fonctionnement de Google G Suite et ses nombreux avantages, ils ont décidé de faire confiance à Loyall pour effectuer la transition. Une fois l’accès aux paramètres DNS restauré, nous avons créé tous les comptes de messagerie de l’équipe et nous avons immédiatement migré les données de messagerie et de calendrier sur G Suite afin d’éviter toute perturbation du trafic des courriers électroniques.<br>Après avoir vérifié que la configuration des courriers électroniques fonctionnait correctement, nous avons commencé la migration de leurs nombreuses données vers Google Drive. <br>Afin de minimiser le coût de la migration, nous avons d’abord commencé par une formation de base sur l’utilisation de Google Drive. A la demande du client, nous avons adapté le processus de migration en leur permettant d'effectuer la migration par leurs propres moyens en les assistant au besoin.<br>Nous avons terminé le déploiement avec des sessions individuelles de formation sur les différents outils inclus dans G Suite et sur la configuration des téléphones mobiles iPhone et Android, et nous les avons conseillés sur les meilleures pratiques à mettre en œuvre pour maximiser l’efficacité et la collaboration au travai."

result: "M. Plutschow et son équipe sont désormais mieux équipés pour gérer les communications entrantes et sortantes, et pour entreprendre leurs activités professionnelles quotidiennes sans avoir à se soucier de faire appel à des intervenants extérieurs. Ceci a également permis de faire des économies financières."
---