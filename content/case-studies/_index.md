---
date: "2017-07-23T11:46:20Z"
draft: false
title: "IT und Cloud Fallstudien | Loyall Erfolgsgeschichten"
meta: "Sehen Sie sich unsere IT Fallstudien von Unternehmen und Schulen an, die wie Ihr Unternehmen eine einfache, ihren Bedürfnissen angepasste, IT-Lösung anstrebten."

page_title: "Fallstudien"
page_subtitle: "Die Zufriedenheit unserer Kunden ist für uns das Wichtigste. Sehen Sie sich einige Fallbeispiele und Erfolgsgeschichten unserer Kunden an und überzeugen Sie sich selbst von unseren Dienstleistungen."


cases: 
  - heading: "Ecole Vinet Privatschule"
    bg_image: "case-vinet"
    url: /case-studies/ecole-vinet-private-school/
    copy: "Als Technologie-Partner arbeiten wir mit der Schule fortlaufend an der Integration von <em>G Suite mit Google Classroom</em> und Technologie-Lösungen im Klassenzimmer."

  - heading: "DMT Treuhand"
    bg_image: "case-dmt"
    url: /case-studies/dmt-treuhand/
    copy: "Wir halfen dem Finanzberatungsunternehmen, sich mit einer Hybrid-Lösung von G Suite und Office 365 auf die Cloud umzustellen."

  - heading: "Plutschow Galerie"
    bg_image: "case-plutschow"
    url: /case-studies/plutschow-gallery/
    copy: "Um Kosten zu reduzieren und effizienter arbeiten zu können hat die Kunstgalerie von ihrem hausinternen Server zu G Suite gewechselt."


---

