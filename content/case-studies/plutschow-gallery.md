---
date: "2017-09-11T11:46:20Z"
draft: false
type: "case"
layout: "case"
css_class: "plutschow"
title: "Plutschow Galerie Fallstudie | G Suite Implementierung"
meta: "Kürzlich verhalfen wir einer Zurcher Kunstgalerie mit der Verlagerung einer IT-Lösung eines Drittanbieters zu einer effizienten G Suite Lösung."

bg_image: "case-plutschow"
logo: "logo_plutschowgallery-min.png"


page_title: "Plutschow Kunstgalerie"
page_subtitle: "G Suite Implementierung und Migrations-Lösung für Kunstgalerie"

client: "Die Plutschow Galerie ist eine moderne Kunstgalerie in Zürich, Schweiz. Wie viele KMUs in der Schweiz vertraute die Plutschow Galerie auf externe Dienstleistungsanbieter für die Betreuung aller IT-Aufgaben, vom Eröffnen eines E-Mail-Kontos bis zum Einrichten des Backups aller Daten."

challenge: "Herr Plutschow, der Besitzer der Galerie, kam auf uns zu, um mehr über G Suite zu erfahren und um zu verstehen, was eine Verlagerung auf diese Cloud-Lösung für sein Unternehmen bedeuten würde.<br>Die spezielle Herausforderung in diesem Fall war die Intregration von bestehenden kunstspezifischen IT Systemen."

solution: "Nachdem wir dem Team erklärt hatten, wie Google’s G Suite funktioniert und welche zahlreichen Vorteile dessen Verwendung mit sich bringt, entschied sich Herr Plutschow, Loyall mit der Migration zu beauftragen. Wir erhielten Zugang zu den DNS-Einstellungen für den Domainnamen des Unternehmens und erstellten alle E-Mail-Konten für das Team, um anschliessend alle E-Mails und Kalenderdaten auf G Suite zu verlagern. Während der Umstellung  funktionierte der E-Mail-Verkehr einwandfrei und wurde zu keinem Zeitpunkt unterbrochen. <br>Sobald wir sichergestellt hatten, dass die E-Mail-Einstellungen richtig funktionierten, begannen wir mit der Migration der zahlreichen Daten auf Google Drive. Um die Kosten dieser Migration auf ein Minimum zu beschränken, fingen wir mit einer Grundlagen-Schulung zur Verwendung von Google Drive an. <br>Auf Wunsch des Kunden haben wir das Tempo des Migrationsprozesses den Mitarbeiterbedürfnissen angepasst und leisteten Unterstützung nach Bedarf.<br>Den Auftrag rundeten wir mit individuellen Schulungen zu den verschiedenen Tools von G Suite ab. Zudem konfigurierten wir Apple und Android Mobiltelefone und gaben Tipps zur Maximierung von Effizienz und optimaler Zusammenarbeit."

result: "Herr Plutschow und sein Team können nun Ihren Geschäftsangelegenheiten effizienter nachgehen, ohne sich Gedanken über einen Drittanbieter machen zu müssen. Zusätzlich konnte er somit auch seine IT-Kosten senken."

---