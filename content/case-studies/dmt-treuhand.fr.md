---
date: "2017-07-23T11:46:20Z"
draft: false
type: "case"
layout: "case"
css_class: "dmt"
title: "Étude de cas DMT Treuhand | G Suite & Office 365"

meta: "Nous avons aidé une firme suisse spécialisée en conseil financier à faire passer vers une solution cloud hybride G Suite / Office 365 qui répond à leurs besoins"

bg_image: "case-dmt"
logo: "logo_dmt_treuhand-min.png"

page_title: "Cabinet de conseil financier DMT Treuhand"
page_subtitle: "Implémentation d’une solution hybride G Suite et Office 365 pour un cabinet de conseil financier"

client: "DMT Treuhand est un cabinet de conseil financier suisse qui offre une large gamme de services pour les particuliers et les professionnels."

challenge: "L’équipe de DMT Treuhand travaille depuis des décennies avec Microsoft Office, mais le lourd fardeau en termes de coûts et de main d’œuvre que représente la maintenance de leur serveur de messagerie et de fichiers a encouragé l’équipe à envisager une solution de cloud.<br>Pour certaines tâches particulières, l'équipe voulait continuer à travailler avec les outils Office et en même temps profiter des avantages de G Suite."

solution: "Pour répondre à leurs besoins, Loyall a proposé l'implémentation d'une solution de cloud hybride: G Suite et Office 365.<br>Ainsi, l’équipe pourrait conserver les avantages de Gmail, Agenda et Cloud Search sur leur domaine et utiliser les outils Office pour leurs tâches spécifiques.<br>Nous avons également mené plusieurs sessions de formations individuelles sur les nouveaux outils et sur l’administration de G Suite durant et après l'implémentation."

result: "L’équipe de DMT Treuhand travaille désormais avec plus de sérénité, sachant que leurs communications et leurs données professionnelles sont sécurisées, facilement accessibles et ultra rapides, ce qui leur permet de répondre plus efficacement aux préoccupations financières de leurs clients."
---

