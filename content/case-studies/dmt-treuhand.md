---
date: "2017-09-11T11:46:20Z"
draft: false
type: "case"
layout: "case"
css_class: "dmt"
title: "DMT Treuhand Fallstudie | G Suite & Office 365"
meta: "Kürzlich verhalfen wir einem Schweizer Finanzberatungsunternehmen mit dem Umstieg zu G Suite/Office 365 zu einer für sie passenden Hybrid-Lösung."

bg_image: "case-dmt"
logo: "logo_dmt_treuhand-min.png"

page_title: "DMT Treuhand Finanzberater"
page_subtitle: "Hybrid-Implementierung von G Suite und Office 365 für ein etabliertes Finanzberatungsunternehmen"


client: "DMT Treuhand ist ein etabliertes Schweizer Finanzberatungsunternehmen, welches über ein breites Angebot an Dienstleistungen für Einzelpersonen sowie Unternehmen verfügt."

challenge: "Das DMT Treuhand-Team arbeitete seit Jahrzehnten mit Microsoft Office. Die hohen Kosten sowie der grosse Arbeitsaufwand für den Unterhalt ihres eigenen E-Mail- und Dateiservers veranlasste das Team eine Cloud-Lösung zu suchen. <br> Für einen Teil der Aufgaben wollten Sie weiterhin mit den Office-Tools arbeiten und gleichzeitig die Vorteile von G Suite für die restlichen Aufgaben nutzen können."

solution: "Loyall präsentierte DMT Treuhand mit einer einzigartiger Hybrid-Lösung: eine Kombination von G Suite und Office 365.<br>So konnte das Team neu Gmail, den Kalender und die Cloud-Suchfunktion für ihre Domain verwenden und gleichzeitig weiterhin die Office-Tools für ihre spezifischen Aufgaben nutzen.<br> Ausserdem führten wir während und nach dem Implementierungsprozess mehrere individuelle Schulungen zu den neuen Tools und der Verwaltung von G Suite durch."

result: "Nun kann sich das DMT Treuhand-Team sicher sein, dass all ihre Daten, sowie die Daten ihrer Klienten optimal gesichert, einfach zugänglich und blitzschnell verfügbar sind, sodass sie den Bedürfnissen ihrer Klienten professionell und effizienter nachkommen können."
---  
 
