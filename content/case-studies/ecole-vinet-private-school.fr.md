---
date: "2017-09-11T11:46:20Z"
draft: false
type: "case"
layout: "case"
css_class: "vinet"

title: "Étude de cas École Vinet | Implémentation de G Suite"
meta: "Loyall soutient continuellement une école privée suisse à introduire les technologies modernes dans les salles de classe et à réduire les coûts informatiques."

bg_image: "case-vinet"
logo: "logo_vinet-min.png"


page_title: "L'École Vinet à Lausanne"
page_subtitle: "Implémentation personnalisée des outils G Suite et du Wi-Fi sur tout le campus"

client: "L’école Vinet est une école privée suisse reconnue avec plus de 175 ans d’expérience dans l’éducation des esprits les plus brillants que Lausanne et ses environs ont à offrir."

challenge: "<p>L’école Vinet avait besoin d’une solution innovante et profitable pour introduire les technologies modernes dans les salles de classe, et en faire bénéficier les étudiants et les enseignants.</p><p>Notre mission consistait en la mise en place d'une solution fiable et économique, favorisant l'essor de l’école et son développement éducatif.</p>"

solution: "La première étape nécessaire au déploiement technologique consistait à mettre en place un réseau Wi-Fi fiable sur tout le campus scolaire. L’implémentation des outils G Suite (anciennement « Google Apps for Education ») quant à elle, permet aux enseignants de l’école Vinet de travailler et de collaborer entre eux. G Suite permet également à l’école de bénéficier d’une plate-forme facile à utiliser pour gérer les comptes des enseignants, ce qui a pour effet de réduire considérablement les coûts engendrés par les intervenants externes. Loyall assiste l’école Vinet dans les initiatives suivantes :<ul class='disk'><li>Déployer des ateliers de formation pour le personnel enseignant et administratif</li><li>S’assurer que l’école puisse bénéficier des dernières fonctionnalités en communiquant de manière proactive</li><li>Fournir un support sur site et à distance ainsi que des propositions d'optimisations</li></ul>"

result: "Grâce à l’implémentation d’une nouvelle technologie et d’un réseau Wi-Fi robuste, les journées au sein de l’école Vinet sont désormais bien plus constructives et fluides, aussi bien pour les étudiants que pour les enseignants."

testim_name: "Bernadette Kaba, Directrice"
testim_text: "Loyall a immédiatement répondu à nos impératifs en comprenant nos défis et proposant des solutions adaptées à nos besoins ainsi qu'en offrant une grande disponibilité et fiabilité dans l'introduction de la technologie dans notre école."

---

