---
date: "2017-07-23T11:46:20Z"
draft: false
title: "Cloud and IT Case Studies | Loyall Success Stories"
meta: "Browse our recent cloud IT case studies from local and global businesses, like yours, who pursued a simplified IT solution for their daily needs."

page_title: "Case Studies"
page_subtitle: "The success of our clients is vital to our mission and one of our greatest pleasures. Check out some of our recent client case studies and success stories to let our work speak for itself."

cases: 
  - heading: "Ecole Vinet private School"
    bg_image: "case-vinet"
    url: /case-studies/ecole-vinet-private-school/
    copy: "As a technology partner, we work contuinuously with the School on the integration of <em>G Suite with Google Classroom</em> and technology in the classroom."

  - heading: "DMT Treuhand"
    bg_image: "case-dmt"
    url: /case-studies/dmt-treuhand/
    copy: "We helped the financial consultant company to move to the cloud using an hybrid solution with G Suite and Office 365."

  - heading: "Plutschow Galerie"
    bg_image: "case-plutschow"
    url: /case-studies/plutschow-gallery/
    copy: "The Art gallery has migrated from their legacy in-house server to G Suite in order to reduce costs and work better."

---