---
date: "2017-07-23T11:46:20Z"
draft: false
type: "case"
layout: "case"
css_class: "vinet"

title: "Ecole Vinet Private School Case Study | G Suite Implementation"
meta: "We recently helped a well-known and well-established Swiss private school introduce technology to the classroom and cut IT costs. Find out how!"

bg_image: "case-vinet"
logo: "logo_vinet-min.png"



page_title: "Ecole Vinet Private School"
page_subtitle: "Tailored G Suite and Wi-Fi implementation campus-wide"

client: "Ecole Vinet is a well-established Swiss private school with over 175 years of success in educating the brightest minds that Lausanne and the surrounding area have to offer."

challenge: "<p>Ecole Vinet found themselves in need of a smart, cost-effective way to introduce modern technology into the classroom, for both their students and teachers’ benefit.</p><p>Our mission was to deliver a cost-effective and reliable solution that allows for future growth as a business and supports the evolving nature of education.</p>"

solution: "The first step to deploying the proper technology was to implement a new and reliable Wi-Fi network within the school campus. Next, the implementation of G Suite (formerly Google Apps for Education) allowed everyone at Ecole Vinet to realize a new way of working and collaborating among the teachers. G Suite has also provided an easy to use platform to manage teachers' accounts within the organization, thus drastically reducing the cost incurred by contracting external service providers. Loyall works to support the Ecole Vinet with ongoing initiatives in the following areas: <ul class='disk'><li>Conducting training workshops for the teaching and administration staff</li><li>Ensuring the school can benefit from the latest features by communicating proactively</li><li>Providing onsite and offsite support as well as continuous improvements</li></ul>"

result: "As a result of the new technology and fresh Wi-Fi connection, the school day at Ecole Vinet has become far more constructive and fluid on both the teaching and learning ends of the spectrum."

testim_name: "Bernadette Kaba, Principal"
testim_text: "The team at Loyall has immediately responded to our imperative requirements by understanding our challenges and suggesting solutions adapted to our needs as well as by offering high availability and reliability in the introduction of technology in the school and the classroom."
---
