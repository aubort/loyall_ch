---
date: "2017-07-23T11:46:20Z"
draft: false
type: "case"
layout: "case"
css_class: "plutschow"
title: "Plutschow Art Gallery Case Study | G Suite Implementation"
meta: "We recently helped a art gallery in Zurich transition from a third-party IT solution to a compact and efficient G Suite solution. Learn More!"

bg_image: "case-plutschow"
logo: "logo_plutschowgallery-min.png"


page_title: "Plutschow Art Gallery"
page_subtitle: "G Suite implementation and migration solutions for a art gallery."

client: "The Plutschow Gallery is a art gallery located in Zürich, Switzerland. Like many SMBs in Switzerland, they relied on an external service provider for anything related to IT, whether it is creating a new email account or running backup of their data."

challenge: "The owner of the Gallery, Mr. Plutschow, approached us to understand more about G Suite and what a move to this cloud solution means for his business. <br>A specific challenge in this case was the integration of their current art management sytem."

solution: "After having shown the team how Google's G Suite works and what the numerous advantages are, they decided to contract Loyall to conduct the transition. After having regained access to the DNS settings for the company's domain name, we created all email accounts for the team and immediately migrated all email and calendar data onto G Suite, avoiding any email traffic disruption.<br>As soon as we made sure that the email setup was working properly, we started the migration of their massive amount of data to Google Drive. In order to keep the cost of the migration minimal, we first started with basic training on the use of Google Drive. At their request, we changed our process so that they could migrate their data at their pace, providing support when needed.<br>We completed the deployment with individual training sessions on the various tools included in G Suite, the setup of Apple and Android mobile devices, and shared best practices to maximize working efficiency and collaboration."

result: "Now, Mr. Plutschow and his team are better equipped to handle all incoming and outgoing communications as well as their daily business processes without worrying about dealing with an outside service provider. Additionally, the move to a Cloud solution allowed him to reduce the IT costs."
---