---
date: "2017-07-23T11:46:20Z"
draft: false
type: "case"
layout: "case"
css_class: "dmt"
title: "DMT Treuhand Financial Case Study | G Suite & Office 365"
meta: "We recently helped a Swiss financial consulting firm transition to a hybrid G Suite/Office 365 solution that met their needs. Learn More!"

bg_image: "case-dmt"
logo: "logo_dmt_treuhand-min.png"


page_title: "DMT Treuhand Financial Consultants"
page_subtitle: "Hybrid G Suite & Office 365 implementation for established financial consulting firm"

client: "DMT Treuhand is an established Swiss financial consulting agency offering a broad range of services to both individuals and businesses."

challenge: "The team at DMT Treuhand had been working with Microsoft Office for decades, and the combined financial and labor burden of maintaining their own email and file server encouraged the team to look at a cloud solution.<br> For part of their tasks, they wanted to keep their Office tools and at the same time benefit from the advantages of G Suite."

solution: "Loyall presented a solution that would respond to their needs, by implementing a hybrid cloud of G Suite and Office 365.<br>This way, the team could use the power of Gmail, Calendar and Cloud Search for their domain, and at the same time continue using the Office tools for their specific tasks. During and after the implementation process, we also conducted multiple individual training sessions on the new tools as well as the administration of G Suite."

result: "Now the team at DMT Treuhand can rest easy knowing that their communications and business data is safe, easily accessible, and lightning fast, allowing them to cater to their client’s financial concerns more efficiently."
---