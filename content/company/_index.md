+++
date = "2017-04-18T11:46:20Z"
draft = true
title = "About Loyall IT Solutions | Swiss Cloud IT Solutions"
meta = "Loyall is a Swiss cloud IT consulting company and IT solutions provider, offering tailored IT services to schools, NGOs, and small businesses. Learn more about what makes us tick."

page_title = "Loyall IT Solutions"
page_subtitle = "Honest and holistic cloud IT solutions for real businesses like yours."

p1 = "Loyall GmbH is a Swiss cloud IT consulting company based in Zurich. We specialize in cloud communication and organization technology for small and medium-sized businesses and educational institutions."

p2 = "Our ultimate mission is to provide you with the best, and most affordable cloud IT solutions to meet both your immediate needs and your long-term business goals, no matter what industry you’re in."

p3 = "At Loyall, there are no one-size-fits-all solutions, and we work to delicately tailor each option to suit your business, not our bank account. We pride ourselves on being transparent and goal-oriented each and every day while providing incredible value and honest, world-class IT support."

team_title = "Meet the Loyall team"
team_intro = ""

pascal_title = "Founder & Chief"
pascal_text = "After having graduated in Media, Management & Engineering, I have had the chance to work and gain experience within non-profit organizations in Africa and Bulgaria. Later I joined Google in Zurich where I was Operations Manager for Google Street View in Europe, before joining a successful Zurich Startup as Technical Project Manager where I was able to collect in-depth technical knowledge. Inbetween I have had the chance to travel - sometimes for longer periods of time - which gave me even more tools and openness to understand customer needs and respond to them in the best way possible. My fluency in French, English and German allow for successful communication. I am very excited to begin a new adventure with Loyall GmbH which was founded in September 2016."


viola_title = "Partner Business Development"
viola_text = "After having completed an apprenticeship including BMS I have started my career in the Recruitment sector at Kelly Services. After that I had the unique opportunity to work at Google in Zurich and was able to contribute to amazing growth and influence the company on many levels. Google Zurich grew from 40 to 1200+ employees in “only” 6 years. In parallel I also completed the Personalfachfrau studies. I then left Google to travel the world and with that expand my horizon. In between two longer travels I have worked at Zurich Insurances and Appway as a Recruiter and am now co-responsible for Business Development, Marketing and Administration at Loyall GmbH. I am fluent in Swiss-German, English, Spanish and French."

+++