---
date: "2017-12-30T11:46:20Z"
draft: false


title: "Cloud Lösungen für Schulen, Unternehmen & NGOs | Loyall"
meta: "Implementieren Sie Cloud IT für Ihre Schule oder Ihr Unternehmen und verbessern Sie die Produktivität und Flexibilität am Arbeitsplatz. Erfahren Sie mehr!"

jumbotron: "Wir bringen Ihre IT in die Cloud"

jumbotron_subtitle: "Verbesserte Kollaboration und erhöhte Sicherheit für Ihr Unternehmen, Ihre Schule oder Nonprofit-Organisation. "


jumbotron_intro: "Bei Loyall bieten wir individuelle, aufeinander abgestimmte Cloud IT Lösungen für Ihr Unternehmen, Ihre Bildungseinrichtung oder Ihre Nonprofit-Organisation an, welche mehr Flexibilität und Sicherheit in Ihren Geschäftsalltag bringen und Sie von anderen Unternehmen in Ihrer Branche abheben lässt."
jumbotron_cfa: "Preise anfragen"

whatiscloud_title: "Was ist die Cloud<br> eigentlich?"

cloud_title: "Wie Sie von der Cloud profitieren werden"
cloud_intro: "Ob Sie die Vorteile von Cloud-Computing gerade erst entdeckt haben oder bereits auf den Cloud-Zug aufgesprungen sind: Wir bieten Ihnen professionelle IT Lösungen, welche auf Ihre individuellen Bedürfnisse und Ziele zugeschnitten sind, um Ihre täglichen Arbeitsabläufe zu optimieren und die Produktivität zu steigern."


features:
    - heading: "Maximale  Sicherheit"
      icon: "icon-fingerprint"
      copy: "Starke Authentifizierung, eine mehrstufige mechanische Sicherung und die Verschlüsselung Ihrer Daten garantieren höchste Sicherheit. Schützen Sie Ihre Daten und setzen Sie Ihre Unternehmensrichtlinien um."

    - heading: "Erleichterte Zusammenarbeit"
      icon: "icon-collaborate"
      copy: "Verbessern Sie die Zusammenarbeit Ihrer Teams mit der neusten und benutzerfreundlichsten Technologie."
    

    - heading: "Mehr Flexibilität"
      icon: "icon-piggybank"
      copy: "Das nutzungsorientierte Preismodel bietet Ihnen maximale Flexibilität, sodass Sie nur für jene Leistungen zahlen, welche Sie aktuell verwenden."

    - heading: "Stets aktuelle Software"
      icon: "icon-cloudsync"
      copy: "Sie haben stets Zugriff auf und arbeiten mit der neusten Software, ohne Sie selber aktualisieren zu müssen."
    
    - heading: "Arbeiten Sie wann und wo Sie wollen"
      icon: "icon-transport"
      copy: "Ganz egal wo Sie sind: Sie können jederzeit, von überall und von jedem beliebigen Gerät auf Ihre Daten zugreifen."

    - heading: "Wettbewerbsvorteil"
      icon: "icon-medal"
      copy: "Mit den modernsten IT Systemen/Tools steigern Sie zusätzlich Ihre Konkurrenzfähigkeit."



    # - heading: "Wiederherstellung im Notfall"
    #   copy: "Die geteilte und dezentralisierte Infrastruktur ermöglicht höchste Verfügbarkeit und eine automatische Datenwiederherstellung."

    # - heading: "Sparen Sie Zeit und Geld"
    #   copy: "Keine teuren Lizenzen oder kostspielige Hardware vor Ort."

    # - heading: "Umweltfreundliches Arbeitsumfeld"
    #   copy: "Umweltfreundliche Datenzentren reduzieren den ökologischen Fussabdruck Ihres Unternehmens."

solutions:
  - heading: "G Suite"
    icon: google
    link_text: "Mehr über G Suite"
    link_url: solutions/g-suite
    copy: "G Suite ist preiswert und sehr leistungsfähig. Die Nutzung von G Suite (vormals Google Apps) kann Ihrem Unternehmen helfen, die Produktivität zu steigern und die Kommunikation zu verbessern."
    
  - heading: "Private Cloud"
    icon: cloud
    link_text: "Mehr über Private Cloud"
    link_url: solutions/private-cloud
    copy: "Verwalten Sie Ihre Daten mit einer robusten Private Cloud-Lösung, welche Ihnen auf Ihre Bedürfnisse zugeschnittenen Speicherplatz bietet."

  - heading: "Chromebooks"
    icon: chrome
    link_text: "Mehr über Chromebooks"
    link_url: solutions/chromebooks
    copy: "Chromebook Computer werden bereits rund um die Welt genutzt. Sie sind kostengünstig, äusserst benutzerfreundlich und einfach einsetzbar. Damit sind sie die optimale Investition für jedes Unternehmen, jede Schule oder Nonprofit-Organisation."

    

# office_title: "Office 365"
# office_text: "Das bewährte Microsoft Office 365 bringt vollumfängliche Produktivität im Gesamtpaket, von E-Mail bis zum Cloud Note-Taking."
# office_link: "Mehr über Office 365"



whatwedo_title: "Was wir für Sie tun"
whatwedo_text: "Loyall bietet Ihnen umfangreiche Kommunikations-Tools und Cloud IT-Lösungen für Ihr Unternehmen, womit Sie die Flexibilität und Produktivität in allen Bereichen Ihrer Organisation steigern können."



why_loyall_title: "Wählen Sie Loyall für Ihre digitale Transformation"
why_loyall_text: "<p>Mit über zehn Jahren Cloud IT-Erfahrung ist Loyall der richtige Partner für Sie um die IT-Bedürfnisse Ihres Unternehmens oder Ihrer Schule abzudecken.</p><p>Unser Geschäftsmodell basiert auf Erfahrung, Transparenz und Loyalität. Gemäss diesen Prinzipien bieten wir unseren Kunden Beratung und Unterstützung zur Realisierung von IT-Lösungen an, welche auf deren individuelle Bedürfnisse zugeschnitten sind. Wir legen Wert auf Neutralität bei der Auswahl von IT-Anbietern. Im Fokus steht stets die beste Lösung für unsere Kunden, und nicht persönliche Interessen oder Verpflichtungen gegenüber einzelnen Anbietern.</p>"

onesize_title: "Individualität statt Einheitslösung"
onesize_text: "Bei Loyall gibt es keine Einheitslösung, die für jedes Unternehmen gleichermassen passt. Jeder Kunde erhält eine individuelle Beratung und IT-Lösungen, welche speziell auf das jeweilige Unternehmen zugeschnitten sind und die entsprechenden längerfristigen Ziele berücksichtigen."

agile_title: "Agile und flexible Techniken"
agile_text: "Egal wie häufig neue Erkenntnisse ans Tageslicht gelangen oder Sie Ihre Pläne ändern: Dank unseren Projektmanagement-Techniken behalten wir den Ablauf stets im Auge und reagieren flexibel auf jede Veränderung."

lang_title: "Ein mehrsprachiges Team"
lang_text: "Unser Team spricht verschiedene Sprachen, einschliesslich Deutsch, Französisch und Englisch. Dies ermöglicht uns, professionell, direkt und effizient mit unseren unterschiedlichen Kunden zusammenzuarbeiten."

meet_cfa: "Mehr über unsere Dienstleistungen"

---
